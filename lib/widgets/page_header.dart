import 'package:flutter/material.dart';

import 'custom_text.dart';

class PageHeader extends StatelessWidget {
  final String title;

  const PageHeader({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 30,
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: CustomText(
              text: title,
              size: 35,
              weight: FontWeight.bold,
              color: Colors.teal),
        ),
      ],
    );
  }
}
