import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/widgets/search_bar/animated_search_bar.dart';

typedef OnRowChange = void Function(int index);

class CustomPaginatedTable extends StatelessWidget {
  const CustomPaginatedTable({
    Key key,
    this.rowsPerPage = PaginatedDataTable.defaultRowsPerPage,
    DataTableSource source,
    List<DataColumn> dataColumns,
    Widget header,
    bool showActions = false,
    List<Widget> actions,
    this.sortColumnIndex,
    this.sortColumnAsc = true,
    this.onRowChanged,
    this.showCheckboxColumn,
    this.showFirstLastButton,
  })  : _source = source,
        _dataColumns = dataColumns,
        _header = header,
        _showActions = showActions,
        _actions = actions,
        assert(
            sortColumnIndex == null ||
                (sortColumnIndex >= 0 && sortColumnIndex < dataColumns.length),
            'Check the sortColumnIndex value'),
        assert(sortColumnAsc != null),
        super(key: key);

  final DataTableSource _source;
  final List<DataColumn> _dataColumns;
  final Widget _header;
  final bool _showActions;
  final List<Widget> _actions;
  final int rowsPerPage;
  final int sortColumnIndex;
  final bool sortColumnAsc;
  final bool showCheckboxColumn;
  final bool showFirstLastButton;

  final OnRowChange onRowChanged;

  DataTableSource get _fetchDataTableSource {
    if (_source != null) {
      return _source;
    }
    return _DefaultSource();
  }

  List<DataColumn> get _fetchDataColumns {
    if (_dataColumns != null) {
      return _dataColumns;
    }
    return _defColumns;
  }

  Widget get _fetchHeader {
    if (_header != null) {
      return _header;
    }
    return const Text('Data with 7 rows per page');
  }

  List<Widget> get _fetchActions {
    if (_showActions && _actions != null) {
      return _actions;
    } else if (!_showActions) {
      return null;
    }
    return _defAction;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: PaginatedDataTable(
              columns: _fetchDataColumns,
              actions: _fetchActions,
              header: _fetchHeader,
              rowsPerPage: rowsPerPage,
              onRowsPerPageChanged: onRowChanged,
              source: _fetchDataTableSource,
              sortColumnIndex: sortColumnIndex,
              showFirstLastButtons:
                  showFirstLastButton != null ? showFirstLastButton : false,
              showCheckboxColumn:
                  showCheckboxColumn != null ? showCheckboxColumn : false,
              sortAscending: sortColumnAsc != null ? true : false,
            ),
          ),
        )
      ],
    );
  }
}

class _DefaultSource extends DataTableSource {
  @override
  DataRow getRow(int index) {
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text('row #$index')),
        DataCell(Text('name #$index')),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => 10;

  @override
  int get selectedRowCount => 0;
}

final _defColumns = <DataColumn>[
  const DataColumn(label: Text('row')),
  const DataColumn(label: Text('name')),
];

final _defAction = <Widget>[
  AnimatedSearchBar(),
  IconButton(
    splashColor: Colors.transparent,
    icon: const Icon(Icons.refresh),
    onPressed: () {
      // _provider.fetchData();
      // _showSBar(context, DataTableConstants.refresh);
    },
  ),
  ElevatedButton.icon(
    onPressed: () {
      print('Thêm mới');
    },
    icon: Icon(Icons.add, size: 18),
    label: Text("Thêm mới"),
    style: ElevatedButton.styleFrom(
      primary: Colors.green,
    ),
  ),
  ElevatedButton.icon(
    onPressed: () {},
    icon: Icon(Icons.delete, size: 18),
    label: Text("Xóa"),
    style: ElevatedButton.styleFrom(
      primary: Colors.red,
    ),
  ),
];
