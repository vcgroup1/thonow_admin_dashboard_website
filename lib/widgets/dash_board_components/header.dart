import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/utilities/common_utils.dart';

class Header extends StatelessWidget {
  const Header({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text('Tổng quan', style: Theme.of(context).textTheme.headline6),
        Spacer(),
        Expanded(child: SearchField()),
        Container(
          margin: EdgeInsets.only(left: defaultPadding),
          padding: EdgeInsets.symmetric(
            horizontal: defaultPadding,
            vertical: defaultPadding / 2,
          ),
          decoration: BoxDecoration(
            color: Colors.teal[100],
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            border: Border.all(color: Colors.teal),
          ),
          child: Row(
            children: [
              Container(
                width: 38,
                height: 38,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                    image: mAuth.currentUser.photoURL != null
                        ? Image.network(mAuth.currentUser.photoURL)
                        : AssetImage("default_avatar.jpg"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: defaultPadding / 2),
                  child: Text('Phan Minh Chau')),
              Icon(Icons.keyboard_arrow_down),
            ],
          ),
        )
      ],
    );
  }
}

class SearchField extends StatelessWidget {
  const SearchField({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
          hintText: 'Tìm kiếm',
          fillColor: Colors.teal[100],
          filled: true,
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide.none,
          ),
          suffixIcon: InkWell(
            onTap: () {},
            child: Container(
              padding: EdgeInsets.all(defaultPadding * 0.75),
              margin: EdgeInsets.symmetric(horizontal: (defaultPadding / 2)),
              decoration: BoxDecoration(
                color: Colors.teal,
                borderRadius: const BorderRadius.all(Radius.circular(10)),
              ),
              child: SvgPicture.asset('search.svg'),
            ),
          )),
    );
  }
}
