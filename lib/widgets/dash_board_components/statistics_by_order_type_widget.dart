import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/servicetype.dart';

import '../custom_text.dart';

class StatisticsByOrderTypeWidget extends StatefulWidget {
  const StatisticsByOrderTypeWidget({
    Key key,
  }) : super(key: key);

  @override
  _StatisticsByOrderTypeWidgetState createState() =>
      _StatisticsByOrderTypeWidgetState();
}

class _StatisticsByOrderTypeWidgetState
    extends State<StatisticsByOrderTypeWidget> {
  int touchedIndex;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: getTotalOrdersByService(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          var list = snapshot.data.docs
              .map((docSnap) => ServiceType.fromFirestore(docSnap))
              .toList();

          var totalOrder = 0;
          list.forEach((element) => totalOrder += element.totalOrders);
          print(totalOrder.toString());

          return Column(
            children: [
              CustomText(
                text: 'Dịch vụ nổi bật',
                size: 18,
                color: Colors.teal,
                weight: FontWeight.w700,
              ),
              SizedBox(
                height: defaultPadding * 2.5,
              ),
              SizedBox(
                height: 200,
                child: buildPieChart(list),
              ),
              SizedBox(
                height: defaultPadding * 2,
              ),
              OrderTypeStatistic(
                serviceType: list[0],
                primaryColor: Colors.blue,
                totalOrders: totalOrder,
              ),
              SizedBox(
                height: defaultPadding / 2,
              ),
              OrderTypeStatistic(
                serviceType: list[1],
                primaryColor: Colors.deepOrange,
                totalOrders: totalOrder,
              ),
              SizedBox(
                height: defaultPadding / 2,
              ),
              OrderTypeStatistic(
                serviceType: list[2],
                primaryColor: Colors.deepPurple,
                totalOrders: totalOrder,
              ),
              SizedBox(
                height: defaultPadding / 2,
              ),
              OrderTypeStatistic(
                serviceType: list[3],
                primaryColor: Colors.green,
                totalOrders: totalOrder,
              ),
              SizedBox(
                height: defaultPadding / 2,
              ),
              OrderTypeStatistic(
                serviceType: list[4],
                primaryColor: Colors.cyan,
                totalOrders: totalOrder,
              ),
              SizedBox(
                height: defaultPadding / 2,
              ),
              OrderTypeStatistic(
                serviceType: list[5],
                primaryColor: Colors.deepOrange,
                totalOrders: totalOrder,
              ),
            ],
          );
        });
  }

  PieChart buildPieChart(List<ServiceType> list) {
    return PieChart(
      PieChartData(
          // pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
          //   setState(() {
          //     final desiredTouch =
          //         pieTouchResponse.touchInput is! PointerExitEvent &&
          //             pieTouchResponse.touchInput is! PointerUpEvent;
          //     if (desiredTouch && pieTouchResponse.touchedSection != null) {
          //       touchedIndex =
          //           pieTouchResponse.touchedSection.touchedSectionIndex;
          //     } else {
          //       touchedIndex = -1;
          //     }
          //   });
          // }),
          borderData: FlBorderData(
            show: false,
          ),
          sectionsSpace: 0,
          centerSpaceRadius: 50,
          startDegreeOffset: -90,
          sections: showingSections(list)),
    );
  }

  List<PieChartSectionData> showingSections(List<ServiceType> list) {
    return List.generate(6, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 20 : 16;
      final double radius = isTouched ? 65 : 60;
      final double widgetSize = isTouched ? 55 : 40;

      var totalOrder = 0;
      list.forEach((element) => totalOrder += element.totalOrders);

      var percent = calculatePercent(list[i].totalOrders, totalOrder);
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Colors.blue,
            value: percent,
            title: percent.toString() + '%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
            badgeWidget: _Badge(
              'ic_tho_sua_chua_nuoc.svg',
              size: widgetSize,
              borderColor: Colors.blue,
            ),
            badgePositionPercentageOffset: .98,
          );
        case 1:
          return PieChartSectionData(
            color: const Color(0xFFFF5722),
            value: percent,
            title: percent.toString() + '%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
            badgeWidget: _Badge(
              'ic_tho_sua_may_tinh.svg',
              size: widgetSize,
              borderColor: const Color(0xFFFF5722),
            ),
            badgePositionPercentageOffset: .98,
          );
        case 2:
          return PieChartSectionData(
            color: const Color(0xFF673AB7),
            value: percent,
            title: percent.toString() + '%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
            badgeWidget: _Badge('ic_tho_sua_dien_gia_dung.svg',
                size: widgetSize, borderColor: const Color(0xFF673AB7)),
            badgePositionPercentageOffset: .98,
          );
        case 3:
          return PieChartSectionData(
            color: const Color(0xFF4CAF50),
            value: percent,
            title: percent.toString() + '%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
            badgeWidget: _Badge(
              'ic_nguoi_giup_viec.svg',
              size: widgetSize,
              borderColor: const Color(0xFF4CAF50),
            ),
            badgePositionPercentageOffset: .98,
          );
        case 4:
          return PieChartSectionData(
            color: const Color(0xFF00ACC1),
            value: percent,
            title: percent.toString() + '%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
            badgeWidget: _Badge(
              'ic_tho_sua_dien_lanh.svg',
              size: widgetSize,
              borderColor: const Color(0xFF00ACC1),
            ),
            badgePositionPercentageOffset: .98,
          );
        case 5:
          return PieChartSectionData(
            color: const Color(0xFFFF7043),
            value: percent,
            title: percent.toString() + '%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
            badgeWidget: _Badge(
              'ic_tho_thong_hut_cong.svg',
              size: widgetSize,
              borderColor: const Color(0xFFFF7043),
            ),
            badgePositionPercentageOffset: .98,
          );
        default:
          return null;
      }
    });
  }
}

class OrderTypeStatistic extends StatelessWidget {
  final ServiceType serviceType;
  final MaterialColor primaryColor;
  final int totalOrders;

  const OrderTypeStatistic({
    Key key,
    this.serviceType,
    this.primaryColor,
    this.totalOrders,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var percent = calculatePercent(serviceType.totalOrders, totalOrders);

    return InkWell(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.all(defaultPadding),
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(defaultPadding),
            ),
            gradient: LinearGradient(colors: [
              primaryColor[600],
              primaryColor,
              primaryColor[400],
            ])),
        child: Row(
          children: [
            SizedBox(
              height: 20,
              width: 20,
              child: SvgPicture.asset(
                  serviceType.drawableName.toString() + '.svg'),
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: defaultPadding,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    serviceType.name,
                    style: TextStyle(
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w600,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: defaultPadding / 4,
                  ),
                  Text(
                    serviceType.totalOrders.toString() + ' đơn đặt',
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: Colors.white70),
                  ),
                ],
              ),
            )),
            CustomText(
              text: percent.toString() + '%',
              color: Colors.white,
              weight: FontWeight.w600,
            ),
          ],
        ),
      ),
    );
  }
}

Stream<QuerySnapshot> getTotalOrdersByService() {
  return db.collection(COLLECTION_SERVICE_TYPE).snapshots();
}

double calculatePercent(int value, int sumValues) {
  return double.parse(((value / sumValues) * 100).toStringAsFixed(1));
}

class _Badge extends StatelessWidget {
  final String svgAsset;
  final double size;
  final Color borderColor;

  const _Badge(
    this.svgAsset, {
    Key key,
    @required this.size,
    @required this.borderColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: PieChart.defaultDuration,
      width: size,
      height: size,
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        border: Border.all(
          color: borderColor,
          width: 2,
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black.withOpacity(.5),
            offset: const Offset(3, 3),
            blurRadius: 3,
          ),
        ],
      ),
      padding: EdgeInsets.all(size * .15),
      child: Center(
        child: SvgPicture.asset(
          svgAsset,
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}
