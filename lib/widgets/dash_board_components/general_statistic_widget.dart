import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:group_button/group_button.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/widgets/dash_board_components/revenue_line_chart_widget.dart';

import '../custom_text.dart';
import '../drop_down_button.dart';

class GeneralStatisticWidget extends StatefulWidget {
  const GeneralStatisticWidget({
    Key key,
  }) : super(key: key);

  @override
  _GeneralStatisticWidgetState createState() => _GeneralStatisticWidgetState();
}

class _GeneralStatisticWidgetState extends State<GeneralStatisticWidget> {
  @override
  Widget build(BuildContext context) {
    List<String> years = ["2021"];
    int optionIndex = 1;
    int range = 30;

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              text: 'Thống kê chung',
              color: Colors.teal,
              size: 18,
              weight: FontWeight.w700,
            ),
            GroupButton(
              isRadio: true,
              spacing: 10,
              selectedButtons: years,
              selectedColor: Colors.teal,
              onSelected: (index, isSelected) {
                setState(() {
                  optionIndex = index;
                  range = getRange(optionIndex);
                });
              },
              buttons: ["Tuần qua", "Tháng qua", "Quý qua"],
            ),
          ],
        ),
        SizedBox(
          height: defaultPadding,
        ),
        GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: 3,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            crossAxisSpacing: defaultPadding,
            childAspectRatio: 410 / 205,
            mainAxisSpacing: defaultPadding,
          ),
          itemBuilder: (context, index) {
            if (index == 0) {
              return Card(
                  semanticContainer: true,
                  child: Stack(children: [
                    Positioned(
                      top: 0,
                      bottom: -25,
                      left: 0,
                      right: 0,
                      child: Align(
                          alignment: FractionalOffset.bottomCenter,
                          child: SvgPicture.asset('statistic_order.svg')),
                    ),
                    StreamBuilder<QuerySnapshot>(
                        stream: getStatisticBy(COLLECTION_ORDER, range),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          var total = snapshot.data.docs.length;

                          return Container(
                            padding: const EdgeInsets.only(
                                left: defaultPadding, top: defaultPadding),
                            alignment: Alignment.topLeft,
                            child: Column(children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: CustomText(
                                  text: 'ĐƠN HÀNG',
                                  size: 14,
                                  weight: FontWeight.w500,
                                  color: HexColor('#FF0034'),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: CustomText(
                                  text: total.toString(),
                                  size: 22,
                                  weight: FontWeight.w800,
                                  color: HexColor('#FF0034'),
                                ),
                              )
                            ]),
                          );
                        }),
                  ]),
                  color: HexColor("#ffc2ce"),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                  ));
            } else if (index == 1) {
              return StreamBuilder<QuerySnapshot>(
                  stream: getStatisticBy(COLLECTION_USER, range),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    var total = snapshot.data.docs.length;

                    return Card(
                        semanticContainer: true,
                        child: Stack(children: [
                          Positioned(
                            top: 0,
                            bottom: -20,
                            left: 0,
                            right: 0,
                            child: Align(
                                alignment: FractionalOffset.bottomCenter,
                                child: SvgPicture.asset('statistic_user.svg')),
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                                left: defaultPadding, top: defaultPadding),
                            alignment: Alignment.topLeft,
                            child: Column(children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: CustomText(
                                  text: 'NGƯỜI DÙNG MỚI',
                                  size: 14,
                                  weight: FontWeight.w500,
                                  color: HexColor('#007BFF'),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: CustomText(
                                  text: total.toString(),
                                  size: 22,
                                  weight: FontWeight.w800,
                                  color: HexColor('#007BFF'),
                                ),
                              )
                            ]),
                          ),
                        ]),
                        color: HexColor("#c2dfff"),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ));
                  });
            } else {
              return StreamBuilder<QuerySnapshot>(
                  stream: getStatisticBy(COLLECTION_WORKER, range),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    var total = snapshot.data.docs.length;

                    return Card(
                        semanticContainer: true,
                        child: Stack(children: [
                          SvgPicture.asset(
                            'statistic_worker.svg',
                            fit: BoxFit.fill,
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                                left: defaultPadding, top: defaultPadding),
                            alignment: Alignment.topLeft,
                            child: Column(children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: CustomText(
                                  text: 'THỢ MỚI',
                                  size: 14,
                                  weight: FontWeight.w500,
                                  color: HexColor('#28A745'),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: CustomText(
                                  text: total.toString(),
                                  size: 22,
                                  weight: FontWeight.w800,
                                  color: HexColor('#28A745'),
                                ),
                              )
                            ]),
                          ),
                        ]),
                        color: HexColor("#cef3d6"),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ));
                  });
            }
          },
        ),
        SizedBox(
          height: defaultPadding,
        ),
        Container(
          padding: EdgeInsets.all(defaultPadding),
          height: 600,
          decoration: BoxDecoration(
            color: Colors.teal[100],
            borderRadius: const BorderRadius.all(Radius.circular(10)),
          ),
          child: RevenueLineChart(),
        )
      ],
    );
  }

  Stream<QuerySnapshot> getStatisticBy(String collection, int range) {
    var currentDate = DateTime.now();
    var previousMonthDate = currentDate.subtract(Duration(days: range));
    return db
        .collection(collection)
        .where('createdAt', isGreaterThanOrEqualTo: previousMonthDate)
        .where('createdAt', isLessThanOrEqualTo: currentDate)
        .snapshots();
  }

  int getRange(int optionIndex) {
    if (optionIndex == 0) {
      return 7;
    } else if (optionIndex == 1) {
      return 30;
    } else {
      return 3 * 30;
    }
  }
}
