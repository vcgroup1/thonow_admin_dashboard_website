import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/helpers/enum_helper_functions.dart';
import 'package:thonow_admin_dashboard_website/models/order.dart';
import 'package:thonow_admin_dashboard_website/models/orderstatus.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';
import 'package:group_button/group_button.dart';
import 'package:thonow_admin_dashboard_website/utilities/common_utils.dart';

import '../drop_down_button.dart';

class RevenueLineChart extends StatefulWidget {
  const RevenueLineChart({Key key}) : super(key: key);

  @override
  _RevenueLineChartState createState() => _RevenueLineChartState();
}

class _RevenueLineChartState extends State<RevenueLineChart> {
  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  bool showAvg = false;
  List<String> years = ['2021'];
  int optionIndex = 1;
  double maxX = 11;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
            padding: const EdgeInsets.only(
                right: 18, left: 12, top: 100, bottom: 12),
            child: StreamBuilder<QuerySnapshot>(
                stream: getOrders(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }

                  var list = snapshot.data.docChanges
                      .map((e) => Order.fromFirestore(e.doc))
                      .toList();

                  print(list.toString());

                  return LineChart(
                    showAvg ? avgData(list) : mainData(list),
                  );
                })),
        SizedBox(
          child: TextButton(
            onPressed: () {
              setState(() {
                showAvg = !showAvg;
              });
            },
            child: Text(
              'Trung bình',
              style: TextStyle(
                  fontSize: 12, color: showAvg ? Colors.teal : Colors.black),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: Column(
            children: [
              CustomText(
                text: 'THỐNG KÊ DOANH THU',
                color: Colors.teal,
                weight: FontWeight.w700,
                size: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GroupButton(
                    isRadio: true,
                    spacing: 10,
                    selectedButtons: years,
                    selectedColor: Colors.teal,
                    onSelected: (index, isSelected) {
                      optionIndex = index;
                      //maxX = getMaxX();
                      setState(() {
                        maxX = getMaxX();
                      });
                      print('maxX' + maxX.toString());
                    },
                    buttons: ["Tuần", "Tháng", "Quý"],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: DropDownButton(
                      list: years,
                      selected: years[0],
                      onChanged: (value) {},
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  LineChartData mainData(List<Order> list) {
    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: const Color(0xff009688),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: const Color(0xff009688),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (value) => const TextStyle(
              color: const Color(0xff009688),
              fontWeight: FontWeight.bold,
              fontSize: 16),
          getTitles: (value) {
            String prefix = 'Th.';
            if (optionIndex == 0) {
              prefix = '';
            } else if (optionIndex == 2) {
              prefix = 'Quý.';
            }

            return prefix + (value.toInt() + 1).toString();
          },
          margin: 10,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
            color: const Color(0xff009688),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            return (value * 5).toString();
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: const Color(0xff00897B), width: 1)),
      minX: 0,
      maxX: maxX,
      minY: 0,
      maxY: 10,
      lineBarsData: [
        LineChartBarData(
          spots: generateFlSpot(list),
          isCurved: true,
          colors: gradientColors,
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }

  LineChartData avgData(List<Order> list) {
    return LineChartData(
      lineTouchData: LineTouchData(enabled: false),
      gridData: FlGridData(
        show: true,
        drawHorizontalLine: true,
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (value) => const TextStyle(
              color: Color(0xff68737d),
              fontWeight: FontWeight.bold,
              fontSize: 16),
          getTitles: (value) {
            return 'Th.' + (value.toInt() + 1).toString();
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
            color: Color(0xff67727d),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            return (value * 100000).toString();
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: const Color(0xff37434d), width: 1)),
      minX: 0,
      maxX: 11,
      minY: 0,
      maxY: 10,
      lineBarsData: [
        LineChartBarData(
          spots: generateAvgFlSpot(list),
          isCurved: true,
          colors: [
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2),
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2),
          ],
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(show: true, colors: [
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2)
                .withOpacity(0.1),
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2)
                .withOpacity(0.1),
          ]),
        ),
      ],
    );
  }

  List<FlSpot> generateFlSpot(List<Order> list) {
    List<FlSpot> spots = [];
    for (var i = 0; i <= getMaxX(); i++) {
      var resultList;
      print(getMaxX().toString());
      if (optionIndex == 0) {
        resultList = filterOrdersByWeek(list, i);
      } else if (optionIndex == 1) {
        resultList = filterOrdersByMonth(list, i);
      } else {
        resultList = filterOrdersByQuarter(list, i);
      }
      var length = resultList != null ? resultList.length : 0;
      spots.add(FlSpot((i).toDouble(), length.toDouble()));
    }
    return spots;
  }

  List<FlSpot> generateAvgFlSpot(List<Order> list) {
    var avg =
        list.map((e) => e.total).reduce((value, element) => value + element) /
            (list.length * 100000);

    print(avg.toDouble().toString());

    List<FlSpot> spots = [];
    for (var i = 0; i <= getMaxX(); i++) {
      spots.add(FlSpot(i.toDouble(), avg));
    }
    return spots;
  }

  Stream<QuerySnapshot> getOrders() {
    return db
        .collection(COLLECTION_ORDER)
        .where('finishDate', isNotEqualTo: null)
        .where('status', isEqualTo: enumToString(OrderStatus.COMPLETED))
        .snapshots();
  }

  List<Order> filterOrdersByMonth(List<Order> orders, int month) {
    if (orders != null && orders.isNotEmpty) {
      return orders
          .where((element) => element.finishDate.month == month)
          .toList();
    }
    return null;
  }

  List<Order> filterOrdersByWeek(List<Order> orders, int week) {
    if (orders != null && orders.isNotEmpty) {
      return orders
          .where((element) => CommonUtil.weekNumber(element.finishDate) == week)
          .toList();
    }
    return null;
  }

  List<Order> filterOrdersByQuarter(List<Order> orders, int quarter) {
    if (orders != null && orders.isNotEmpty) {
      return orders
          .where(
              (element) => CommonUtil.getQuarter(element.finishDate) == quarter)
          .toList();
    }
    return null;
  }

  double getMaxX() {
    if (optionIndex == 0) {
      return 51;
    } else if (optionIndex == 1) {
      return 11;
    } else {
      return 3;
    }
  }
}
