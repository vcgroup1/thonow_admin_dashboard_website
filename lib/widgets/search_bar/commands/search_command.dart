import 'package:thonow_admin_dashboard_website/widgets/change_notifier.dart';
import 'package:thonow_admin_dashboard_website/widgets/search_bar/algo/string_search.dart';
import 'package:thonow_admin_dashboard_website/widgets/search_bar/commands/base_command.dart';

class SearchCommand extends GenericNotifier with BaseCommand {
  SearchCommand() {
    _articles = _articleIDList;
  }

  List<String> showSearchResults(String searchTerm) {
    _searchedResults = StringSearch(_articles, searchTerm).relevantResults();

    notify();
    return _searchedResults;
  }

  List<String> get searchedResults => _searchedResults;

  // ************************** INTERNALS

  List<String> _articles;

  List<String> get _articleIDList {
    var articles = <String>[];

    return articles;
  }

  List<String> _searchedResults = [];

  //final _searchOps = locator<SearchOperations>();
}
