import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:provider/provider.dart';
import 'package:thonow_admin_dashboard_website/helpers/enumerators.dart';
import 'package:thonow_admin_dashboard_website/providers/app_provider.dart';
import 'package:thonow_admin_dashboard_website/rounting/route_names.dart';
import 'package:thonow_admin_dashboard_website/services/navigation_service.dart';

import '../../locator.dart';
import '../../rounting/route_names.dart';
import 'navbar_item.dart';

class NavBar extends StatefulWidget {
  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  List<bool> selected = [true, false, false, false, false, false];
  void select(int n) {
    for (int i = 0; i < 6; i++) {
      if (i != n) {
        selected[i] = false;
      } else {
        selected[i] = true;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppProvider appProvider = Provider.of<AppProvider>(context);

    return Container(
      height: 400.0,
      child: Column(
        children: [
          NavBarItem(
            icon: FontAwesome5Solid.igloo,
            title: 'Tổng quan',
            active: selected[0],
            touched: () {
              setState(() {
                select(0);
              });
              appProvider.changeCurrentPage(DisplayedPage.HOME);
              locator<NavigationService>().navigateTo(HomeRoute);
              print(HomeRoute);
            },
          ),
          NavBarItem(
            icon: FontAwesome5Solid.receipt,
            title: 'Đơn hàng',
            active: selected[1],
            touched: () {
              setState(() {
                select(1);
              });
              appProvider.changeCurrentPage(DisplayedPage.ORDERS);
              locator<NavigationService>().navigateTo(OrdersRoute);
              print(OrdersRoute);
            },
          ),
          NavBarItem(
            icon: FontAwesome5Solid.newspaper,
            title: 'Tin tức',
            active: selected[2],
            touched: () {
              setState(() {
                select(2);
              });
              appProvider.changeCurrentPage(DisplayedPage.NEWS);
              locator<NavigationService>().navigateTo(NewsRoute);
            },
          ),
          NavBarItem(
            icon: FontAwesome5Solid.tags,
            title: 'Khuyến mãi',
            active: selected[3],
            touched: () {
              setState(() {
                select(3);
              });
              appProvider.changeCurrentPage(DisplayedPage.OFFERS);
              locator<NavigationService>().navigateTo(OffersRoute);
            },
          ),
          NavBarItem(
            icon: FontAwesome5Solid.users,
            active: selected[4],
            title: 'Người dùng',
            touched: () {
              setState(() {
                select(4);
              });
              appProvider.changeCurrentPage(DisplayedPage.USERS);
              locator<NavigationService>().navigateTo(UsersRoute);
              print(appProvider.currentPage.toString());
            },
          ),
          NavBarItem(
            icon: Icons.engineering,
            active: selected[5],
            title: "Thợ",
            touched: () {
              setState(() {
                select(5);
              });
              appProvider.changeCurrentPage(DisplayedPage.WORKERS);
              locator<NavigationService>().navigateTo(WorkersRoute);
              print(appProvider.currentPage.toString());
            },
          ),
        ],
      ),
    );
  }
}
