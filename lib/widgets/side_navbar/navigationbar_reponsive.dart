import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:thonow_admin_dashboard_website/widgets/side_navbar/navigation_bar.dart';

class NavigationBarResponsive extends StatelessWidget {
  const NavigationBarResponsive({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      breakpoints: ScreenBreakpoints(tablet: 600, desktop: 1460, watch: 300),
      desktop: NavigationBar(),
      mobile: NavigationBar(),
    );
  }
}
