
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:provider/provider.dart';
import 'package:thonow_admin_dashboard_website/widgets/side_navbar/navbar.dart';
import 'package:thonow_admin_dashboard_website/widgets/side_navbar/navbar_item.dart';
import 'package:thonow_admin_dashboard_website/widgets/side_navbar/navbar_logo.dart';

import '../../locator.dart';
import '../../providers/auth.dart';
import '../../rounting/route_names.dart';
import '../../services/navigation_service.dart';

class NavigationBar extends StatelessWidget {
  const NavigationBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);

    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: 255.0,
        color: Colors.teal,
        child: Stack(
          children: [
            NavbarLogo(),
            Align(
              alignment: Alignment.center,
              child: NavBar(),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: NavBarItem(
                icon: Feather.log_out,
                title: 'Đăng xuất',
                active: false,
                touched: () {
                  return showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text('Đăng xuất?'),
                        content: Text('Bạn có chắc chắn muốn đăng xuất chứ?'),
                        actions: [
                          TextButton(
                            style: TextButton.styleFrom(
                              textStyle: TextStyle(color: Colors.teal),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('Hủy'),
                          ),
                          TextButton(
                            style: TextButton.styleFrom(
                              textStyle: TextStyle(color: Colors.teal),
                            ),
                            onPressed: () async {
                              await authProvider.signOut();
                              locator<NavigationService>()
                                  .globalNavigateTo(LoginRoute, context);
                            },
                            child: Text('Đăng xuất'),
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
