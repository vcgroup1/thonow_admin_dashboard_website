import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NavbarLogo extends StatelessWidget {
  const NavbarLogo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      child: Center(
        child: SvgPicture.asset(
          'text_logo.svg',
          fit: BoxFit.none,
          height: 70,
          width: 190,
          
        ),
      ),
    );
  }
}
