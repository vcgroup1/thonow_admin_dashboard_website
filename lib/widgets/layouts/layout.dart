import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/rounting/route_names.dart';
import 'package:thonow_admin_dashboard_website/rounting/router.dart';
import 'package:thonow_admin_dashboard_website/services/navigation_service.dart';
import 'package:thonow_admin_dashboard_website/widgets/side_navbar/navigationbar_reponsive.dart';

import '../../locator.dart';

class LayoutTemplate extends StatelessWidget {
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      backgroundColor: Colors.white,
      body: Row(
        children: [
          NavigationBarResponsive(),
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Navigator(
                    key: locator<NavigationService>().navigatorKey,
                    onGenerateRoute: generateRoute,
                    initialRoute: HomeRoute,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
