import 'package:flutter/material.dart';

class DropDownButton<T> extends StatefulWidget {
  final List<T> list;
  final ValueChanged<T> onChanged;
  final T selected;

  DropDownButton({
    Key key,
    @required this.list,
    @required this.onChanged,
    @required this.selected,
  }) : super(key: key);

  @override
  _DropDownButtonState<T> createState() => _DropDownButtonState<T>();
}

class _DropDownButtonState<T> extends State<DropDownButton> {
  List<DropdownMenuItem<T>> _dropdownMenuItems;
  T _selectedItem;

  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(widget.list);
    _selectedItem =
        widget.selected == null ? _dropdownMenuItems[0].value : widget.selected;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      items: _dropdownMenuItems,
      value: _selectedItem,
      onChanged: (value) {
        widget.onChanged(value);
        setState(() {
          _selectedItem = value;
        });
      },
    );
  }

  List<DropdownMenuItem<T>> buildDropdownMenuItems(List list) {
    List<DropdownMenuItem<T>> items = [];
    for (Object object in list) {
      items.add(DropdownMenuItem(
        child: Text(object.toString()),
        value: object,
      ));
    }
    return items;
  }
}
