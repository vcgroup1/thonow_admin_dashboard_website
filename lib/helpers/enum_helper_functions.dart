// source: https://cloud-trends.medium.com/advanced-enums-in-flutter-a8f2e2702ffd
String enumToString(Object o) => o.toString().split('.').last;

T enumFromString<T>(String key, List<T> values) =>
    values.firstWhere((v) => key == enumToString(v), orElse: () => null);
