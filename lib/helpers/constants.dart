// firebase
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:intl/intl.dart';

final Future<FirebaseApp> initialization = Firebase.initializeApp();

FirebaseFirestore db = FirebaseFirestore.instance;
FirebaseAuth mAuth = FirebaseAuth.instance;
FirebaseStorage storage = FirebaseStorage.instance;
DateFormat simpleDateFormat = DateFormat('dd/MM/yyyy');
DateFormat orderDateFormat = DateFormat('dd/MM/yyyy HH:mm');
NumberFormat simpleCurrencyFormat =
    NumberFormat.currency(locale: 'vi_VN', symbol: 'VNĐ ');
const STORAGE_BUCKET = 'gs://thonow-android-app.appspot.com/';
const ID_GENERATOR_FORMAT = '{20.}';
const defaultPadding = 16.0;

// Firestore Collection
const COLLECTION_ADMIN = "admins";
const COLLECTION_USER = "users";
const COLLECTION_USER_GEOLOCATION = "user_geolocations";
const LIB_COUNTRY_CODE = "lib_country_code";
const COLLECTION_OFFER = "offers";
const COLLECTION_NEWS = "news";
const COLLECTION_SERVICE = "services";
const COLLECTION_SERVICE_TYPE = "service_type";
const COLLECTION_WORKER = "workers";
const COLLECTION_WORKER_GEOLOCATION = "worker_geolocations";
const COLLECTION_TOKEN = "tokens";
const COLLECTION_ORDER = "orders";
const COLLECTION_MESSAGE = "messages";
const COLLECTION_LAST_MESSAGE = "last_message";

const googleSearchPercent = 0.3;

class DataTableConstants {
  DataTableConstants._();

  static const String col_action = 'Tác vụ';

  // COLUMN USERS
  static const String header_users = "Danh sách người dùng";
  static const String col_user_uid = 'UID';
  static const String col_user_displayName = 'Tên hiển thị';
  static const String col_user_phone = 'Số điện thoại';
  static const String col_user_status = 'Trạng thái';

  // COLUMN NEWS
  static const String header_news = "Danh sách tin tức";
  static const String col_news_id = 'ID';
  static const String col_news_title = 'Tiêu đề';
  static const String col_news_thumbnail = 'Ảnh thumbnail';
  static const String col_news_createAt = 'Ngày tạo';

  // COLUMN OFFER
  static const String header_offer = "Danh sách khuyến mãi";
  static const String col_offer_id = 'ID';
  static const String col_offer_title = 'Tiêu đề';
  static const String col_offer_thumbnail = 'Ảnh thumbnail';
  static const String col_offer_createAt = 'Ngày tạo';

  // COLUMN ORDER
  static const String header_order = "Danh sách đơn hàng";
  static const String col_order_id = 'ID';
  static const String col_order_service = 'Dịch vụ';
  static const String col_order_customer = 'Khách hàng';
  static const String col_order_worker = 'Thợ làm việc';
  static const String col_order_status = 'Trạng thái';
  static const String col_order_working_date = 'Ngày làm việc';
  static const String col_order_total = 'Tổng tiền';

  // COLUMN WORKERS
  static const String header_worker = "Danh sách thợ";
  static const String col_worker_uid = 'UID';
  static const String col_worker_displayName = 'Tên hiển thị';
  static const String col_worker_phone = 'Số điện thoại';
  static const String col_worker_isVerified = 'Đã xác minh';
  static const String col_worker_isActive = 'Trạng thái';
  static const String col_worker_discount_charge = 'Tổng phí chiết khấu';

  // COLUMN ORDER_DETAILS
  static const String col_order_detail_id = "STT";
  static const String col_order_detail_service = "Dịch vụ";
  static const String col_order_detail_quantity = "Số lượng";
  static const String col_order_detail_unit_price = "Đơn giá";
  static const String col_order_detail_subtotal = "Tổng";
}
