import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CommonUtil {
  static String generatefilePath(String folderName, String fileName) {
    final now = DateTime.now();
    return folderName + '/' + fileName + '_' + '$now';
  }

  static Image loadImage(
      BuildContext context, String url, double height, double width) {
    return Image.network(
      url,
      height: height,
      width: width,
      fit: BoxFit.fill,
      loadingBuilder: (BuildContext context, Widget child,
          ImageChunkEvent loadingProgress) {
        if (loadingProgress == null) return child;
        return Center(
          child: CircularProgressIndicator(
            value: loadingProgress.expectedTotalBytes != null
                ? loadingProgress.cumulativeBytesLoaded /
                    loadingProgress.expectedTotalBytes
                : null,
          ),
        );
      },
    );
  }

  static Color hexToColor(String hexString, {String alphaChannel = 'FF'}) {
    return Color(int.parse(hexString.replaceFirst('#', '0x$alphaChannel')));
  }

  static int getQuarter(DateTime dateTime) {
    return (dateTime.month / 3).ceil();
  }

  static int weekNumber(DateTime date) {
    int dayOfYear = int.parse(DateFormat("D").format(date));
    return ((dayOfYear - date.weekday + 10) / 7).floor();
  }
}
