import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:thonow_admin_dashboard_website/locator.dart';
import 'package:thonow_admin_dashboard_website/providers/auth.dart';
import 'package:thonow_admin_dashboard_website/rounting/route_names.dart';
import 'package:thonow_admin_dashboard_website/services/navigation_service.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';
import 'package:thonow_admin_dashboard_website/widgets/loading.dart';

class LoginPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);

    return Container(
        decoration: BoxDecoration(
          color: Colors.teal,
          image: new DecorationImage(
              image: AssetImage('background.png'), fit: BoxFit.fill),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: SvgPicture.asset(
                'text_logo.svg',
                fit: BoxFit.none,
                height: 70,
                width: 190,
              ),
              // Image.asset('text_logo.png',
              //     height: 70, width: 190, fit: BoxFit.none),
            ),
            Align(
              alignment: Alignment.center,
              child: authProvider.status == Status.Authenticating
                  ? Loading()
                  : Scaffold(
                      key: _key,
                      backgroundColor: Colors.transparent,
                      body: Center(
                          child: Card(
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0, 3),
                                    blurRadius: 24)
                              ]),
                          height: 400,
                          width: 400,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomText(
                                text: "Đăng nhập",
                                size: 22,
                                color: Colors.teal,
                                weight: FontWeight.bold,
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 30),
                                child: Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: TextFormField(
                                      controller: authProvider.email,
                                      decoration: InputDecoration(
                                        labelText: 'Email',
                                        hintText: "Nhập email của bạn",
                                        icon: Icon(
                                          Icons.account_box,
                                        ),
                                        border: OutlineInputBorder(),
                                        suffixIcon: Icon(
                                          Icons.check_circle,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 30),
                                child: Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: TextFormField(
                                      controller: authProvider.password,
                                      obscureText: true,
                                      decoration: InputDecoration(
                                        labelText: 'Password',
                                        hintText: "Nhập mật khẩu của bạn",
                                        icon: Icon(
                                          Icons.lock,
                                        ),
                                        border: OutlineInputBorder(),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 25,
                              ),
                              FractionallySizedBox(
                                  widthFactor: 0.9,
                                  child: ElevatedButton(
                                    onPressed: () async {
                                      if (!await authProvider.signIn()) {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(SnackBar(
                                          behavior: SnackBarBehavior.floating,
                                          content: Text('Đăng nhập thất bại'),
                                        ));
                                        return;
                                      }
                                      authProvider.clearController();

                                      locator<NavigationService>()
                                          .globalNavigateTo(
                                              PageControllerRoute, context);
                                    },
                                    child: Text('Đăng nhập'),
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.teal[800],
                                      padding: EdgeInsets.all(20),
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      )),
                    ),
            )
          ],
        ));
  }
}
