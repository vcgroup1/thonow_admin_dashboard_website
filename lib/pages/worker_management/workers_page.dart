import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:credit_card/flutter_credit_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:provider/provider.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_notifier/worker_data_notifier.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_table_source/worker_data_table_source.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/service.dart';
import 'package:thonow_admin_dashboard_website/models/servicetype.dart';
import 'package:thonow_admin_dashboard_website/models/worker.dart';
import 'package:thonow_admin_dashboard_website/utilities/common_utils.dart';
import 'package:thonow_admin_dashboard_website/utilities/screen_size.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_dialog.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';
import 'package:thonow_admin_dashboard_website/widgets/data_table/custom_paginated_datatable.dart';
import 'package:thonow_admin_dashboard_website/widgets/search_bar/animated_search_bar.dart';

class WorkersPage extends StatefulWidget {
  const WorkersPage({Key key}) : super(key: key);

  @override
  _WorkersPageState createState() => _WorkersPageState();
}

class _WorkersPageState extends State<WorkersPage> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<WorkerDataNotifier>(
      create: (context) => WorkerDataNotifier(),
      child: Padding(
        padding: EdgeInsets.all(20),
        child: _InternalWidget(),
      ),
    );
  }
}

class _InternalWidget extends StatelessWidget {
  const _InternalWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<WorkerDataNotifier>();
    final _list = _provider.list;

    if (_list.isEmpty) {
      return const SizedBox.shrink();
    }

    final _dataSource = WorkerDataTableSource(
      context: context,
      data: _list,
      onRowSelect: (index) => _showDetails(context, _list[index], _provider),
      onShowBankAccountInfo: (index) =>
          _showBankAccountInformation(context, _list[index], _provider),
    );

    AlertDialog setActiveForMultipleWorkers(
        BuildContext context, List<Worker> _selectedWorker, bool active) {
      return AlertDialog(
        title: Text('Cho phép thợ hoạt động?'),
        content: Text('Bạn có chắc chắn muốn thực hiện tác vụ trên ' +
            _selectedWorker.length.toString() +
            ' tài khoản thợ này chứ?'),
        actions: [
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Hủy'),
          ),
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () async {
              _provider
                  .setActiveForMultipleWorker(_selectedWorker, active)
                  .onError((error, stackTrace) =>
                      _showSnackbar(context, error.toString()));
              Navigator.of(context).pop();
              _provider.fetchData();
            },
            child: Text('Tiếp tục'),
          ),
        ],
      );
    }

    AlertDialog setIdentifyForMultipleWorkers(
        BuildContext context, List<Worker> _selectedWorker, bool isVerified) {
      return AlertDialog(
        title: Text('Xác minh danh tính?'),
        content: Text('Bạn có chắc chắn muốn thực hiện tác vụ trên ' +
            _selectedWorker.length.toString() +
            ' tài khoản thợ này chứ?'),
        actions: [
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Hủy'),
          ),
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () async {
              _provider.setVerifiedForMultipleWorker(
                  _selectedWorker, isVerified);
              Navigator.of(context).pop();
              _provider.fetchData();
            },
            child: Text('Tiếp tục'),
          ),
        ],
      );
    }

    void searchByText(String query) {
      _provider.search(context, query);
    }

    return CustomPaginatedTable(
      dataColumns: _colGen(_dataSource, _provider),
      header: const Text(DataTableConstants.header_worker),
      showActions: true,
      source: _dataSource,
      sortColumnIndex: _provider.sortColumnIndex,
      sortColumnAsc: _provider.sortAsc,
      onRowChanged: (index) => _provider.rowsPerPage = index,
      actions: <Widget>[
        AnimatedSearchBar(
          onChanged: (value) => searchByText(value),
        ),
        IconButton(
          splashColor: Colors.transparent,
          icon: const Icon(
            Icons.refresh,
            color: Colors.teal,
          ),
          onPressed: () {
            _provider.fetchData();
            _showSnackbar(context, "Dữ liệu đã được cập nhật!");
          },
        ),
        ElevatedButton.icon(
          onPressed: () {
            var _selectedUser = _dataSource.getSelectedObjects;

            if (_selectedUser.length > 0) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return setActiveForMultipleWorkers(
                        context, _selectedUser, false);
                  });
            }
          },
          icon: Icon(
            Icons.lock,
            size: 18,
            color: Colors.white,
          ),
          label: Text("Khoá tài khoản"),
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
          ),
        ),
        ElevatedButton.icon(
          onPressed: () {
            var _selectedUser = _dataSource.getSelectedObjects;

            if (_selectedUser.length > 0) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return setActiveForMultipleWorkers(
                        context, _selectedUser, true);
                  });
            }
          },
          icon: Icon(
            Icons.lock_open,
            size: 18,
            color: Colors.white,
          ),
          label: Text("Mở Khoá tài khoản"),
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
          ),
        ),
        ElevatedButton.icon(
          onPressed: () {
            var _selectedUser = _dataSource.getSelectedObjects;

            if (_selectedUser.length > 0) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return setIdentifyForMultipleWorkers(
                        context, _selectedUser, true);
                  });
            }
          },
          icon: Icon(
            FontAwesome5Solid.id_card,
            size: 18,
            color: Colors.white,
          ),
          label: Text("Xác minh danh tính"),
          style: ElevatedButton.styleFrom(
            primary: Colors.deepPurple,
          ),
        ),
      ],
      rowsPerPage: _provider.rowsPerPage,
    );
  }

  void _showDetails(
      BuildContext context, Worker worker, WorkerDataNotifier provider) {
    showDialog<bool>(
      context: context,
      builder: (_) => CustomDialog(
        showPadding: false,
        child: WorkerDetailInfo(
          worker: worker,
          provider: provider,
        ),
      ),
    );
  }

  void _showBankAccountInformation(
      BuildContext context, Worker worker, WorkerDataNotifier provider) {
    showDialog<bool>(
      context: context,
      builder: (_) => CustomDialog(
        showPadding: false,
        child: BankAccountInformation(
          worker: worker,
          provider: provider,
        ),
      ),
    );
  }

  List<DataColumn> _colGen(
      WorkerDataTableSource _src, WorkerDataNotifier _provider) {
    return <DataColumn>[
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_worker_uid,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_worker_uid,
        onSort: (columnIndex, ascending) {
          _sort<String>((worker) => worker.documentID, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_worker_displayName,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_worker_displayName,
        onSort: (columnIndex, ascending) {
          _sort<String>((worker) => worker.displayName, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_worker_phone,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_worker_phone,
        onSort: (columnIndex, ascending) {
          _sort<String>((worker) => worker.phoneNumber, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_worker_isVerified,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_worker_isVerified,
      ),
      DataColumn(
          label: CustomText(
            text: DataTableConstants.col_worker_isActive,
            color: Colors.teal,
            size: 15,
            weight: FontWeight.bold,
          ),
          tooltip: DataTableConstants.col_worker_isActive),
      DataColumn(
        label: Center(
          child: CustomText(
            text: DataTableConstants.col_action,
            color: Colors.teal,
            size: 15,
            weight: FontWeight.bold,
          ),
        ),
        tooltip: DataTableConstants.col_action,
      ),
      DataColumn(label: Text('')),
    ];
  }

  void _sort<T>(Comparable<T> Function(Worker worker) getField, int colIndex,
      bool asc, WorkerDataTableSource _src, WorkerDataNotifier _provider) {
    _src.sort<T>(getField, asc);
    _provider.sortAsc = asc;
    _provider.sortColumnIndex = colIndex;
  }

  void _showSnackbar(BuildContext context, String s) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(s),
      duration: const Duration(milliseconds: 2000),
    ));
  }
}

class BankAccountInformation extends StatelessWidget {
  final Worker worker;
  final WorkerDataNotifier provider;

  const BankAccountInformation({Key key, @required this.worker, this.provider})
      : assert(worker != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> cardWidgetList = [];
    final _width = ScreenQueries.instance.width(context);

    if (worker.creditCardInfos != null && worker.creditCardInfos.isNotEmpty) {
      worker.creditCardInfos.forEach((element) {
        var widget = Padding(
            padding: EdgeInsets.all(10),
            child: CreditCardWidget(
              width: 500,
              height: 200,
              cardNumber: element.accountNumber,
              expiryDate: 'Unknown',
              cardHolderName: element.accountName,
              cvvCode: '',
              showBackView: true, //true when you want to show cvv(back) view
            ));
        cardWidgetList.add(widget);
      });
    }

    return Container(
        width: _width * 0.5,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: CustomText(
                      text: 'Thông tin tài khoản thợ',
                      size: 17,
                      color: Colors.teal,
                      weight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: CloseButton(
                    color: Colors.teal,
                  ),
                )
              ],
            ),
            Divider(indent: 8),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: cardWidgetList,
              ),
            ),
          ],
        ));
  }
}

class WorkerDetailInfo extends StatefulWidget {
  final Worker worker;
  final WorkerDataNotifier provider;

  const WorkerDetailInfo({Key key, @required this.worker, this.provider})
      : assert(worker != null),
        super(key: key);

  @override
  _WorkerDetailInfoState createState() => _WorkerDetailInfoState();
}

class _WorkerDetailInfoState extends State<WorkerDetailInfo> {
  @override
  Widget build(BuildContext context) {
    final _width = ScreenQueries.instance.width(context);
    final _height = ScreenQueries.instance.height(context);

    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

    Worker worker = widget.worker;

    bool flagInsert;

    String currentImageUrl =
        worker.idCard != null ? worker.idCard.frontIdCardUrl : '';

    if (worker == null) {
      flagInsert = true;
      worker = new Worker();
    } else {
      flagInsert = false;
    }

    return Container(
      width: _width * 0.6,
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: CustomText(
                    text: 'Thông tin tài khoản thợ',
                    size: 17,
                    color: Colors.teal,
                    weight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                child: CloseButton(
                  color: Colors.teal,
                ),
              )
            ],
          ),
          Divider(indent: 8.0),
          Container(
            width: _width * 0.6,
            height: _height * 0.7,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Container(
                              width: 150,
                              height: 150,
                              child: (widget.worker.photoUrl == null ||
                                      widget.worker.photoUrl.isNotEmpty)
                                  ? Image.asset(
                                      'default_image.png',
                                      width: 150,
                                      height: 150,
                                      fit: BoxFit.fill,
                                    )
                                  : CommonUtil.loadImage(context,
                                      widget.worker.photoUrl, 150, 150),
                            )),
                        Expanded(
                          flex: 4,
                          child: Column(
                            children: [
                              SizedBox(
                                  width: _width * 0.55 - 150,
                                  child: TextFormField(
                                    initialValue: widget.worker == null
                                        ? '«ID sẽ được tự động tạo»'
                                        : widget.worker.documentID,
                                    decoration: InputDecoration(
                                      labelText: 'ID',
                                      enabled: !(widget.worker != null),
                                      border: OutlineInputBorder(),
                                      suffixIcon: Icon(
                                        Icons.check_circle,
                                      ),
                                    ),
                                  )),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.verified_user_rounded,
                                      color: widget.worker.verified
                                          ? Colors.green
                                          : Colors.grey[400],
                                    ),
                                    CustomText(
                                      text: widget.worker.verified
                                          ? 'Đã xác minh'
                                          : 'Chưa xác minh',
                                      color: widget.worker.verified
                                          ? Colors.green
                                          : Colors.grey[400],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(8, 0, 0, 8),
                                child: Row(
                                  children: [
                                    widget.worker.male
                                        ? Icon(
                                            FontAwesome.mars,
                                            color: Colors.blue,
                                          )
                                        : Icon(
                                            FontAwesome.venus,
                                            color: Colors.pink,
                                          ),
                                    CustomText(
                                      text: widget.worker.male ? 'Nam' : 'Nữ',
                                      color: widget.worker.male
                                          ? Colors.blue
                                          : Colors.pink,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(8, 15, 8, 5),
                            child: TextFormField(
                              initialValue: widget.worker == null
                                  ? ''
                                  : widget.worker.displayName,
                              onSaved: (newValue) =>
                                  worker.displayName = newValue,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Tên thợ không được rỗng';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                icon: Icon(Icons.badge),
                                labelText: 'Tên hiển thị',
                                border: OutlineInputBorder(),
                                suffixIcon: Icon(
                                  Icons.check_circle,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: TextFormField(
                              initialValue:
                                  worker == null ? '' : worker.phoneNumber,
                              onSaved: (newValue) =>
                                  worker.phoneNumber = newValue,
                              decoration: InputDecoration(
                                icon: Icon(Icons.contact_phone),
                                labelText: 'Số điện thoại',
                                border: OutlineInputBorder(),
                                enabled: false,
                                suffixIcon: Icon(
                                  Icons.check_circle,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: SizedBox(
                        width: _width * 0.6,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                          child: TextFormField(
                            initialValue:
                                (worker == null || worker.address == null)
                                    ? ''
                                    : worker.address.address,
                            decoration: InputDecoration(
                              icon: Icon(Icons.my_location),
                              labelText: 'Địa chỉ',
                              border: OutlineInputBorder(),
                              enabled: false,
                              suffixIcon: Icon(
                                Icons.check_circle,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Divider(indent: 8.0),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: CustomText(
                              text: 'Hoạt động trên các dịch vụ: ',
                              weight: FontWeight.bold,
                              size: 15),
                        ),
                        StreamBuilder<QuerySnapshot>(
                          stream: getService(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }

                            List<Widget> actionsChip = [];
                            snapshot.data.docs.forEach((element) {
                              ServiceType service =
                                  ServiceType.fromFirestore(element);
                              actionsChip.add(createActionChip(service));
                            });

                            return Row(
                              children: actionsChip,
                            );
                          },
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: CustomText(
                              text: 'Rating: ',
                              weight: FontWeight.bold,
                              size: 15),
                        ),
                        RatingBar.builder(
                          itemSize: 16,
                          initialRating: worker.rating,
                          minRating: 0,
                          direction: Axis.horizontal,
                          ignoreGestures: true,
                          allowHalfRating: true,
                          itemCount: 5,
                          onRatingUpdate: (value) {},
                          itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            size: 9,
                            color: Colors.amber,
                          ),
                        ),
                      ],
                    ),
                    Divider(indent: 8.0),
                    Row(
                      children: [
                        Expanded(
                            flex: 2,
                            child: Column(
                              children: [
                                Container(
                                  width: 350,
                                  child: currentImageUrl.isNotEmpty
                                      ? CommonUtil.loadImage(
                                          context, currentImageUrl, 200, 260)
                                      : Image.asset(
                                          'image_place_holder.png',
                                          width: 260,
                                          height: 150,
                                          fit: BoxFit.fill,
                                        ),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.only(left: 5),
                                      child: OutlinedButton(
                                          onPressed: () {
                                            setState(() {
                                              currentImageUrl =
                                                  worker.idCard.frontIdCardUrl;
                                            });
                                          },
                                          child: Text('Mặt trước')),
                                    ),
                                    Container(
                                      alignment: Alignment.centerRight,
                                      padding: EdgeInsets.only(right: 5),
                                      child: OutlinedButton(
                                          onPressed: () {
                                            setState(() {
                                              currentImageUrl =
                                                  worker.idCard.backIdCardUrl;
                                            });
                                          },
                                          child: Text('Mặt sau')),
                                    ),
                                  ],
                                )
                              ],
                            )),
                        Expanded(
                          flex: 3,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: SizedBox(
                                    width: _width * 0.55 - 200,
                                    child: TextFormField(
                                      initialValue: widget.worker.idNumber,
                                      onSaved: (newValue) =>
                                          worker.idNumber = newValue,
                                      decoration: InputDecoration(
                                        icon: Icon(
                                          FontAwesome5Solid.id_card,
                                        ),
                                        labelText: 'Số CMND/CCCD',
                                        border: OutlineInputBorder(),
                                        suffixIcon: Icon(
                                          Icons.check_circle,
                                        ),
                                      ),
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(top: 10),
                      height: 50,
                      child: ElevatedButton.icon(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            widget.provider.update(context, worker);
                          }
                          Navigator.of(context).pop();
                          widget.provider.fetchData();
                        },
                        icon: Icon(Icons.save, size: 18),
                        label: Text("Lưu thông tin"),
                        style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40),
                            )),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  String getServiceTypeList() {
    var serviceTypeList = widget.worker.serviceTypeList;
    String serviceTypeListText = '';
    if (serviceTypeList != null && serviceTypeList.isNotEmpty) {
      var serviceType = [];
      serviceTypeList.forEach((element) {
        element.get().then((value) {
          Service service = Service.fromFirestore(value);
          serviceType.add(service);
          serviceTypeListText += service.name + '\n';
        });
      });
    }
    return serviceTypeListText;
  }

  Stream<QuerySnapshot> getService() {
    List<String> serviceTypeIdList = [];
    widget.worker.serviceTypeList.forEach((element) {
      serviceTypeIdList.add(element.id);
    });

    return db
        .collection(COLLECTION_SERVICE_TYPE)
        .where('id', whereIn: serviceTypeIdList)
        .snapshots();
  }

  Widget createActionChip(ServiceType serviceType) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: ActionChip(
        label: CustomText(
          text: serviceType.name,
          color: Colors.blue[700],
          size: 14,
        ),
        onPressed: () {},
        avatar: SvgPicture.asset(
          serviceType.drawableName + '.svg',
          width: 64,
          height: 64,
        ),
        backgroundColor: Colors.grey[200],
      ),
    );
  }
}
