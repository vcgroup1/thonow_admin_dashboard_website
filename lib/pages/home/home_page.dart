import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/widgets/dash_board_components/general_statistic_widget.dart';
import 'package:thonow_admin_dashboard_website/widgets/dash_board_components/header.dart';
import 'package:thonow_admin_dashboard_website/widgets/dash_board_components/statistics_by_order_type_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
      padding: EdgeInsets.all(defaultPadding),
      child: Column(
        children: [
          Header(),
          SizedBox(
            height: defaultPadding,
          ),
          Row(children: [
            Expanded(
              flex: 5,
              child: GeneralStatisticWidget(),
            ),
            SizedBox(
              width: defaultPadding,
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.all(defaultPadding),
                height: 850,
                decoration: BoxDecoration(
                  color: Colors.teal[100],
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                ),
                child: StatisticsByOrderTypeWidget(),
              ),
            ),
          ])
        ],
      ),
    ));
  }
}
