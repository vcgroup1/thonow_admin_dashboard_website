import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_notifier/order_data_notifier.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_table_source/order_data_table_source.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_table_source/order_detail_data_table_source.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/order.dart';
import 'package:thonow_admin_dashboard_website/models/orderdetail.dart';
import 'package:thonow_admin_dashboard_website/models/service.dart';
import 'package:thonow_admin_dashboard_website/models/user.dart';
import 'package:thonow_admin_dashboard_website/models/worker.dart';
import 'package:thonow_admin_dashboard_website/utilities/screen_size.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_dialog.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';
import 'package:thonow_admin_dashboard_website/widgets/data_table/custom_paginated_datatable.dart';
import 'package:thonow_admin_dashboard_website/widgets/search_bar/animated_search_bar.dart';
import 'package:thonow_admin_dashboard_website/models/orderstatus.dart';
import 'package:thonow_admin_dashboard_website/models/ordertype.dart';

class OrdersPage extends StatefulWidget {
  OrdersPage({Key key}) : super(key: key);

  @override
  _OrdersPageState createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<OrderDataNotifier>(
      create: (context) => new OrderDataNotifier(),
      child: Padding(
        padding: EdgeInsets.all(20),
        child: _InternalWidget(),
      ),
    );
  }
}

class _InternalWidget extends StatelessWidget {
  const _InternalWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<OrderDataNotifier>();
    var _list = _provider.list;

    if (_list == null) {
      return const SizedBox.shrink();
    }

    final _dataSource = OrderDataTableSource(
      context: context,
      data: _list,
      onRowSelect: (index) => _showDetails(context, _list[index], _provider),
    );

    void searchByText(String query) {
      _provider.search(context, query);
    }

    return CustomPaginatedTable(
      source: _dataSource,
      header: const Text(DataTableConstants.header_order),
      showActions: true,
      sortColumnIndex: _provider.sortColumnIndex,
      sortColumnAsc: _provider.sortAsc,
      onRowChanged: (index) => _provider.rowsPerPage = index,
      rowsPerPage: _provider.rowsPerPage,
      actions: <Widget>[
        AnimatedSearchBar(
          onChanged: (value) => searchByText(value),
        ),
        IconButton(
          splashColor: Colors.transparent,
          icon: const Icon(
            Icons.refresh,
            color: Colors.teal,
          ),
          onPressed: () {
            _provider.fetchData();
            _showSnackbar(context, "Dữ liệu đã được cập nhật!");
          },
        ),
      ],
      dataColumns: _colGen(_dataSource, _provider),
    );
  }

  List<DataColumn> _colGen(
      OrderDataTableSource _src, OrderDataNotifier _provider) {
    return <DataColumn>[
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_order_id,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_order_id,
        onSort: (columnIndex, ascending) {
          _sort<String>((order) => order.documentID, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_order_service,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_order_service,
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_order_customer,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_order_customer,
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_order_worker,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_order_worker,
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_order_status,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_order_status,
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_order_working_date,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_order_working_date,
        onSort: (columnIndex, ascending) {
          _sort<DateTime>((order) => order.workingDate, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_order_total,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_order_total,
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_action,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: "Tác vụ",
      )
    ];
  }

  void _sort<T>(Comparable<T> Function(Order order) getField, int colIndex,
      bool asc, OrderDataTableSource _src, OrderDataNotifier _provider) {
    _src.sort<T>(getField, asc);
    _provider.sortAsc = asc;
    _provider.sortColumnIndex = colIndex;
  }

  void _showSnackbar(BuildContext context, String s) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(s),
      duration: const Duration(milliseconds: 2000),
    ));
  }

  void _showDetails(
      BuildContext context, Order order, OrderDataNotifier provider) async {
    await showDialog<bool>(
      context: context,
      builder: (_) => CustomDialog(
          showPadding: false,
          child: OrderDetailInfo(
            order: order,
            provider: provider,
          )),
    );
  }
}

class OrderDetailInfo extends StatelessWidget {
  final Order order;
  final OrderDataNotifier provider;

  const OrderDetailInfo({Key key, this.order, this.provider}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _width = ScreenQueries.instance.width(context);

    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        width: _width * 0.6,
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: CustomText(
                      text: 'Thông tin đơn hàng',
                      size: 17,
                      color: Colors.teal,
                      weight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: CloseButton(
                    color: Colors.teal,
                  ),
                )
              ],
            ),
            Divider(indent: 8.0),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Mã đơn hàng: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        CustomText(
                          text: order.documentID,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Loại đơn hàng: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        CustomText(
                          text: order.type.title,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Trạng thái: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        Chip(
                          label: CustomText(
                            text: order.status.title,
                            color: Colors.white,
                            size: 14,
                          ),
                          avatar: Icon(
                            FontAwesome5Solid.box,
                            size: 14,
                            color: Colors.white,
                          ),
                          backgroundColor: HexColor(order.status.color),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Ngày tạo đơn: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        CustomText(
                          text: orderDateFormat.format(order.workingDate),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Thời gian làm việc: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        CustomText(
                          text: orderDateFormat.format(order.workingDate),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Ngày hoàn thành: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        CustomText(
                          text: order.finishDate != null
                              ? orderDateFormat.format(order.finishDate)
                              : 'N/A',
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Địa chỉ làm việc: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        CustomText(
                          text: order.workingAddress.address,
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8),
                        child: Row(
                          children: [
                            CustomText(
                              text: 'Khách hàng: ',
                              weight: FontWeight.bold,
                              color: Colors.teal,
                            ),
                            StreamBuilder<DocumentSnapshot>(
                              stream: getObject(order.customer),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }

                                User customer =
                                    User.fromFirestore(snapshot.data);
                                return CustomText(
                                    text: customer != null
                                        ? customer.displayName
                                        : '');
                              },
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Row(
                          children: [
                            CustomText(
                              text: 'Thợ làm việc: ',
                              weight: FontWeight.bold,
                              color: Colors.teal,
                            ),
                            order.worker != null
                                ? StreamBuilder<DocumentSnapshot>(
                                    stream: getObject(order.worker),
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.waiting) {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }

                                      if (!snapshot.hasData) {
                                        return CustomText(
                                            text: 'Đang tìm kiếm');
                                      }

                                      Worker worker =
                                          Worker.fromFirestore(snapshot.data);

                                      return CustomText(
                                          text: worker.displayName);
                                    },
                                  )
                                : CustomText(text: 'Chưa có'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Chi tiết đơn hàng: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: SizedBox(
                            child: DataTable(
                              columnSpacing: 45,
                              columns: [
                                DataColumn(
                                    label: CustomText(
                                  text: DataTableConstants.col_order_detail_id,
                                  color: Colors.teal,
                                  size: 15,
                                  weight: FontWeight.bold,
                                )),
                                DataColumn(
                                    label: CustomText(
                                  text: DataTableConstants
                                      .col_order_detail_service,
                                  color: Colors.teal,
                                  size: 15,
                                  weight: FontWeight.bold,
                                )),
                                DataColumn(
                                    label: CustomText(
                                  text: DataTableConstants
                                      .col_order_detail_quantity,
                                  color: Colors.teal,
                                  size: 15,
                                  weight: FontWeight.bold,
                                )),
                                DataColumn(
                                    label: CustomText(
                                  text: DataTableConstants
                                      .col_order_detail_unit_price,
                                  color: Colors.teal,
                                  size: 15,
                                  weight: FontWeight.bold,
                                )),
                                DataColumn(
                                    label: CustomText(
                                  text: DataTableConstants
                                      .col_order_detail_subtotal,
                                  color: Colors.teal,
                                  size: 15,
                                  weight: FontWeight.bold,
                                )),
                              ],
                              rows: _genOrderDetailsRows(order.orderDetails),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Chi tiết các phí: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: SizedBox(
                            width: 500,
                            child: Column(
                              children: _genOrderChargeRows(order),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        CustomText(
                          text: 'Ghi chú: ',
                          weight: FontWeight.bold,
                          color: Colors.teal,
                        ),
                        CustomText(
                          text: order.note != null ? order.note : 'Không',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Stream<QuerySnapshot> getServices(List<OrderDetail> orderDetails) {
    List<String> serviceIdList = [];

    orderDetails.forEach((element) {
      serviceIdList.add(element.service.id);
    });

    return db
        .collection(COLLECTION_SERVICE)
        .where('id', whereIn: serviceIdList)
        .snapshots();
  }

  Stream<DocumentSnapshot> getObject(DocumentReference docRef) {
    return docRef.snapshots();
  }

  List<DataRow> _genOrderDetailsRows(List<OrderDetail> orderDetails) {
    List<DataRow> dataRows = [];
    for (var index = 0; index < orderDetails.length; index++) {
      OrderDetail orderDetail = orderDetails[index];
      dataRows.add(DataRow.byIndex(
        index: index,
        cells: <DataCell>[
          DataCell(Text((index + 1).toString())),
          DataCell(StreamBuilder<DocumentSnapshot>(
            stream: getObject(orderDetail.service),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              Service service = Service.fromFirestore(snapshot.data);

              return Text(service.name);
            },
          )),
          DataCell(Text(orderDetail.quantity.toString())),
          DataCell(Text(orderDetail.unitPrice.toString())),
          DataCell(Text(orderDetail.subtotal.toString())),
        ],
      ));
    }
    return dataRows;
  }

  List<Widget> _genOrderChargeRows(Order order) {
    List<Widget> dataRows = [];
    dataRows.add(Row(
      children: [
        CustomText(
          text: 'Chi phí ước tính: ',
          weight: FontWeight.bold,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: CustomText(
            text: order.estimatedFee.toString(),
          ),
        )
      ],
    ));
    dataRows.add(Row(
      children: [
        CustomText(
          text: 'Chi phí di chuyển: ',
          weight: FontWeight.bold,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: CustomText(
            text: order.movingExpense.toString(),
          ),
        )
      ],
    ));
    dataRows.add(Row(
      children: [
        CustomText(
          text: 'Chi phí quá giờ: ',
          weight: FontWeight.bold,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: CustomText(
            text: order.overtimeFee.toString(),
          ),
        )
      ],
    ));
    dataRows.add(Row(
      children: [
        CustomText(
          text: 'Tiền tip: ',
          weight: FontWeight.bold,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: CustomText(
            text: order.tip.toString(),
          ),
        )
      ],
    ));
    dataRows.add(Row(
      children: [
        CustomText(
          text: 'Chi phí khác: ',
          weight: FontWeight.bold,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: CustomText(
            text: order.otherExpense.toString(),
          ),
        )
      ],
    ));
    dataRows.add(Row(
      children: [
        CustomText(
          text: 'Tổng chi phí: ',
          weight: FontWeight.bold,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: CustomText(
            text: order.total.toString(),
          ),
        )
      ],
    ));
    return dataRows;
  }
}
