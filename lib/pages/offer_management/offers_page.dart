import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:provider/provider.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_notifier/offer_data_notifier.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_table_source/offer_data_table_source.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/offer.dart';
import 'package:thonow_admin_dashboard_website/utilities/common_utils.dart';
import 'package:thonow_admin_dashboard_website/utilities/screen_size.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_dialog.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';
import 'package:thonow_admin_dashboard_website/widgets/data_table/custom_paginated_datatable.dart';
import 'package:thonow_admin_dashboard_website/widgets/drop_down_button.dart';
import 'package:thonow_admin_dashboard_website/widgets/search_bar/animated_search_bar.dart';

class OffersPage extends StatefulWidget {
  OffersPage({Key key}) : super(key: key);

  @override
  _OffersPageState createState() => _OffersPageState();
}

class _OffersPageState extends State<OffersPage> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<OfferDataNotifier>(
      create: (context) => OfferDataNotifier(),
      child: Padding(
        padding: EdgeInsets.all(20),
        child: _InternalWidget(),
      ),
    );
  }
}

class _InternalWidget extends StatelessWidget {
  const _InternalWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<OfferDataNotifier>();
    final _list = _provider.list;

    if (_list != null && _list.isEmpty) {
      return const SizedBox.shrink();
    }

    final _dataSource = OfferDataTableSource(
      context: context,
      data: _list,
      onRowSelect: (index) => _showDetails(context, _list[index], _provider),
    );

    AlertDialog deleteMultipleOffers(
        BuildContext context, List<Offer> _selectedOffer) {
      return AlertDialog(
        title: Text('Xoá dữ liệu?'),
        content: Text('Bạn có chắc chắn muốn thực hiện tác vụ trên ' +
            _selectedOffer.length.toString() +
            ' khuyến mãi này chứ?'),
        actions: [
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Hủy'),
          ),
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () async {
              _provider.deleteMultipleOffer(_selectedOffer);
              Navigator.of(context).pop();
              _provider.fetchData();
            },
            child: Text('Tiếp tục'),
          ),
        ],
      );
    }

    void searchByText(String query) {
      _provider.search(context, query);
    }

    return CustomPaginatedTable(
      source: _dataSource,
      header: const Text(DataTableConstants.header_offer),
      showActions: true,
      sortColumnIndex: _provider.sortColumnIndex,
      sortColumnAsc: _provider.sortAsc,
      onRowChanged: (index) => _provider.rowsPerPage = index,
      rowsPerPage: _provider.rowsPerPage,
      actions: <Widget>[
        AnimatedSearchBar(
          onChanged: (value) => searchByText(value),
        ),
        IconButton(
          splashColor: Colors.transparent,
          icon: const Icon(
            Icons.refresh,
            color: Colors.teal,
          ),
          onPressed: () {
            _provider.fetchData();
            _showSnackbar(context, "Dữ liệu đã được cập nhật!");
          },
        ),
        ElevatedButton.icon(
          onPressed: () {
            _showDetails(context, null, _provider);
          },
          icon: Icon(Icons.add, size: 18),
          label: Text("Thêm mới"),
          style: ElevatedButton.styleFrom(
            primary: Colors.green,
          ),
        ),
        ElevatedButton.icon(
          onPressed: () {
            List<Offer> _selectedOffer = _dataSource.getSelectedObjects;

            if (_selectedOffer.length > 0) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return deleteMultipleOffers(context, _selectedOffer);
                  });
            }
          },
          icon: Icon(
            Icons.delete,
            size: 18,
            color: Colors.white,
          ),
          label: Text("Xoá"),
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
          ),
        ),
      ],
      dataColumns: _colGen(_dataSource, _provider),
    );
  }

  List<DataColumn> _colGen(
      OfferDataTableSource _src, OfferDataNotifier _provider) {
    return <DataColumn>[
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_offer_id,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_offer_id,
        onSort: (columnIndex, ascending) {
          _sort<String>((offer) => offer.documentID, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_offer_thumbnail,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_offer_thumbnail,
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_offer_title,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_offer_title,
        onSort: (columnIndex, ascending) {
          _sort<String>(
              (offer) => offer.title, columnIndex, ascending, _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_offer_createAt,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_offer_createAt,
        onSort: (columnIndex, ascending) {
          _sort<DateTime>((offer) => offer.createdAt, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_action,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: "Tác vụ",
      )
    ];
  }

  void _sort<T>(Comparable<T> Function(Offer offer) getField, int colIndex,
      bool asc, OfferDataTableSource _src, OfferDataNotifier _provider) {
    _src.sort<T>(getField, asc);
    _provider.sortAsc = asc;
    _provider.sortColumnIndex = colIndex;
  }

  void _showSnackbar(BuildContext context, String s) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(s),
      duration: const Duration(milliseconds: 2000),
    ));
  }

  void _showDetails(
      BuildContext context, Offer offer, OfferDataNotifier provider) async {
    await showDialog<bool>(
      context: context,
      builder: (_) => CustomDialog(
          showPadding: false,
          child: OfferDetailInfo(
            key: key,
            offer: offer,
            provider: provider,
          )),
    );
  }
}

class OfferDetailInfo extends StatefulWidget {
  final Key key;
  final Offer offer;
  final OfferDataNotifier provider;

  const OfferDetailInfo({this.key, this.offer, this.provider})
      : super(key: key);

  @override
  _OfferDetailInfoState createState() => _OfferDetailInfoState();
}

class _OfferDetailInfoState extends State<OfferDetailInfo> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  HtmlEditorController controller = HtmlEditorController();
  String newImageUrl = '';

  @override
  Widget build(BuildContext context) {
    final _width = ScreenQueries.instance.width(context);
    var options = ['Dành cho thợ', 'Dành cho khách'];

    Offer offer = widget.offer;

    bool flagInsert;

    if (offer == null) {
      flagInsert = true;
      offer = new Offer();
    } else {
      flagInsert = false;
    }

    void doSaveOrUpdate() async {
      _formKey.currentState.save();

      if (flagInsert) {
        // insert
        widget.provider.insertOffer(context, offer);
      } else {
        // modify
        widget.provider.updateOffer(context, offer);
      }
      Navigator.of(context).pop();
      widget.provider.fetchData();
    }

    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          width: _width * 0.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: CustomText(
                        text: Offer != null
                            ? "Cập nhật thông tin"
                            : "Thêm mới thông tin",
                        size: 17,
                        color: Colors.teal,
                        weight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    child: CloseButton(
                      color: Colors.teal,
                    ),
                  )
                ],
              ),
              Divider(indent: 8.0),
              // begin
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 12, 8, 12),
                child: Row(
                  children: [
                    Column(
                      children: [
                        StreamBuilder<String>(
                          stream: downloadUrl(offer.thumbnail).asStream(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            } else if (snapshot.connectionState ==
                                ConnectionState.none) {
                              return Image.asset(
                                'image_place_holder.png',
                                width: 150,
                                height: 150,
                                fit: BoxFit.fill,
                              );
                            }

                            String imgUrl = snapshot.data;

                            if (newImageUrl.isEmpty && imgUrl == null) {
                              return Image.asset(
                                'image_place_holder.png',
                                width: 150,
                                height: 150,
                                fit: BoxFit.fill,
                              );
                            }

                            offer.thumbnail =
                                imgUrl != null ? imgUrl : newImageUrl;
                            return CommonUtil.loadImage(
                                context,
                                imgUrl != null ? imgUrl : newImageUrl,
                                150,
                                150);
                          },
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                                width: 70,
                                alignment: Alignment.centerLeft,
                                child: IconButton(
                                    onPressed: () {
                                      uploadToStorage(offer);
                                    },
                                    icon: Icon(
                                      Icons.add_a_photo,
                                      color: Colors.green,
                                    ))),
                            Container(
                                width: 70,
                                alignment: Alignment.centerRight,
                                child: IconButton(
                                    onPressed: () {},
                                    icon: Icon(
                                      Icons.remove_circle,
                                      color: Colors.red,
                                    ))),
                          ],
                        )
                      ],
                    ),
                    Expanded(
                        child: Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: TextFormField(
                        initialValue: widget.offer == null
                            ? '«ID sẽ được tự động tạo»'
                            : widget.offer.documentID,
                        decoration: InputDecoration(
                          labelText: 'ID',
                          enabled: !(Offer != null),
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.check_circle,
                          ),
                        ),
                      ),
                    )),
                  ],
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                      child: TextFormField(
                        initialValue:
                            widget.offer == null ? '' : widget.offer.title,
                        onSaved: (newValue) => offer.title = newValue,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Tiêu đề không được rỗng';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          icon: Icon(FontAwesome5Solid.heading),
                          labelText: 'Tiêu đề',
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.check_circle,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                      child: TextFormField(
                        initialValue: offer == null ? '' : offer.subtitle,
                        onSaved: (newValue) => offer.subtitle = newValue,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Tiêu đề phụ không được rỗng';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          icon: Icon(FontAwesome.text_height),
                          labelText: 'Tiêu đề phụ',
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.check_circle,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: CustomText(
                            text: 'Dành cho : ',
                            weight: FontWeight.normal,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: DropDownButton(
                            list: options,
                            selected:
                                (offer.isForWorker != null && offer.isForWorker)
                                    ? options[0]
                                    : options[1],
                            onChanged: (value) {
                              print(value);
                              offer.isForWorker =
                                  (options.indexOf(value) == 0 ? true : false);
                              print(offer.isForWorker ? 'true' : 'false');
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                      child: TextFormField(
                        initialValue: offer == null ? '' : offer.code,
                        onSaved: (newValue) => offer.code = newValue,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Code khuyến mãi không được rỗng';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          icon: Icon(FontAwesome.hashtag),
                          labelText: 'Mã code',
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.check_circle,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: HtmlEditor(
                  controller: controller,
                  htmlToolbarOptions: HtmlToolbarOptions(
                    renderBorder: true,
                    toolbarType: ToolbarType.nativeScrollable,
                  ),
                  htmlEditorOptions: HtmlEditorOptions(
                    initialText: offer.content,
                    hint: "Nhập nội dung ở đây..",
                  ),
                  otherOptions: OtherOptions(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                    ),
                    height: 150,
                  ),
                  callbacks: Callbacks(
                    onChange: (value) {
                      offer.content = value;
                    },
                  ),
                ),
              ),

              // end
              Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 10),
                height: 50,
                child: ElevatedButton.icon(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      doSaveOrUpdate();
                    }
                  },
                  icon: Icon(Icons.save, size: 18),
                  label: Text("Lưu thông tin"),
                  style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40),
                      )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void chooseImage({@required Function(File file) onSelected}) {
    InputElement uploadInput = FileUploadInputElement()..accept = 'image/*';
    uploadInput.click();

    uploadInput.onChange.listen((event) {
      final file = uploadInput.files.first;
      final reader = FileReader();

      reader.readAsDataUrl(file);
      reader.onLoadEnd.listen((event) {
        onSelected(file);
      });
    });
  }

  void uploadToStorage(Offer offer) {
    chooseImage(
      onSelected: (file) {
        String path = CommonUtil.generatefilePath(COLLECTION_OFFER, file.name);
        storage.refFromURL(STORAGE_BUCKET).child(path).putBlob(file).then((_) {
          _.ref.getDownloadURL().then((value) {
            db
                .collection(COLLECTION_OFFER)
                .doc(offer.documentID)
                .update({'thumbnail': value}).then((value1) {
              this.setState(() {
                offer.thumbnail = value;
              });
            }).onError((error, stackTrace) {
              this.setState(() {
                offer.thumbnail = value;
                newImageUrl = value;
              });
            });
          });
        });
      },
    );
  }

  Future<String> downloadUrl(String photoUrl) {
    if (photoUrl != null && photoUrl.contains(STORAGE_BUCKET)) {
      return storage.ref(photoUrl).getDownloadURL();
    }
    return storage.refFromURL(STORAGE_BUCKET).child(photoUrl).getDownloadURL();
  }
}
