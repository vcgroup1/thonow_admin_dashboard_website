import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:provider/provider.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_notifier/news_data_notifier.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_table_source/news_data_table_source.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/news.dart';
import 'package:thonow_admin_dashboard_website/utilities/common_utils.dart';
import 'package:thonow_admin_dashboard_website/utilities/screen_size.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_dialog.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';
import 'package:thonow_admin_dashboard_website/widgets/data_table/custom_paginated_datatable.dart';
import 'package:thonow_admin_dashboard_website/widgets/drop_down_button.dart';
import 'package:thonow_admin_dashboard_website/widgets/search_bar/animated_search_bar.dart';

class NewsPage extends StatefulWidget {
  NewsPage({Key key}) : super(key: key);

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<NewsDataNotifier>(
      create: (context) => NewsDataNotifier(),
      child: Padding(
        padding: EdgeInsets.all(20),
        child: _InternalWidget(),
      ),
    );
  }
}

class _InternalWidget extends StatelessWidget {
  const _InternalWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<NewsDataNotifier>();
    final _newsList = _provider.newsList;

    if (_newsList != null && _newsList.isEmpty) {
      return const SizedBox.shrink();
    }

    final _dataSource = NewsDataTableSource(
      context: context,
      data: _newsList,
      onRowSelect: (index) =>
          _showDetails(context, _newsList[index], _provider),
    );

    AlertDialog deleteMultipleNews(
        BuildContext context, List<News> _selectedNews) {
      return AlertDialog(
        title: Text('Xoá dữ liệu?'),
        content: Text('Bạn có chắc chắn muốn thực hiện tác vụ trên ' +
            _selectedNews.length.toString() +
            ' tin tức này chứ?'),
        actions: [
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Hủy'),
          ),
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () async {
              _provider.deleteMultipleNews(_selectedNews);
              Navigator.of(context).pop();
              _provider.fetchData();
            },
            child: Text('Tiếp tục'),
          ),
        ],
      );
    }

    void searchByText(String query) {
      _provider.search(context, query);
    }

    return CustomPaginatedTable(
      source: _dataSource,
      header: const Text(DataTableConstants.header_news),
      showActions: true,
      sortColumnIndex: _provider.sortColumnIndex,
      sortColumnAsc: _provider.sortAsc,
      onRowChanged: (index) => _provider.rowsPerPage = index,
      rowsPerPage: _provider.rowsPerPage,
      actions: <Widget>[
        AnimatedSearchBar(
          onChanged: (value) => searchByText(value),
        ),
        IconButton(
          splashColor: Colors.transparent,
          icon: const Icon(
            Icons.refresh,
            color: Colors.teal,
          ),
          onPressed: () {
            _provider.fetchData();
            _showSnackbar(context, "Dữ liệu đã được cập nhật!");
          },
        ),
        ElevatedButton.icon(
          onPressed: () {
            _showDetails(context, null, _provider);
          },
          icon: Icon(Icons.add, size: 18),
          label: Text("Thêm mới"),
          style: ElevatedButton.styleFrom(
            primary: Colors.green,
          ),
        ),
        ElevatedButton.icon(
          onPressed: () {
            List<News> _selectedNews = _dataSource.getSelectedObjects;

            if (_selectedNews.length > 0) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return deleteMultipleNews(context, _selectedNews);
                  });
            }
          },
          icon: Icon(
            Icons.delete,
            size: 18,
            color: Colors.white,
          ),
          label: Text("Xoá"),
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
          ),
        ),
      ],
      dataColumns: _colGen(_dataSource, _provider),
    );
  }

  List<DataColumn> _colGen(
      NewsDataTableSource _src, NewsDataNotifier _provider) {
    return <DataColumn>[
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_news_id,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_news_id,
        onSort: (columnIndex, ascending) {
          _sort<String>((news) => news.documentID, columnIndex, ascending, _src,
              _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_news_thumbnail,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_news_thumbnail,
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_news_title,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_news_title,
        onSort: (columnIndex, ascending) {
          _sort<String>(
              (news) => news.title, columnIndex, ascending, _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_news_createAt,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_news_createAt,
        onSort: (columnIndex, ascending) {
          _sort<DateTime>((news) => news.createdAt, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_action,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: "Tác vụ",
      )
    ];
  }

  void _sort<T>(Comparable<T> Function(News news) getField, int colIndex,
      bool asc, NewsDataTableSource _src, NewsDataNotifier _provider) {
    _src.sort<T>(getField, asc);
    _provider.sortAsc = asc;
    _provider.sortColumnIndex = colIndex;
  }

  void _showSnackbar(BuildContext context, String s) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(s),
      duration: const Duration(milliseconds: 2000),
    ));
  }

  void _showDetails(
      BuildContext context, News news, NewsDataNotifier provider) async {
    await showDialog<bool>(
      context: context,
      builder: (_) => CustomDialog(
          showPadding: false,
          child: NewsDetailInfo(
            news: news,
            provider: provider,
          )),
    );
  }
}

class NewsDetailInfo extends StatefulWidget {
  final Key key;
  final News news;
  final NewsDataNotifier provider;

  const NewsDetailInfo({this.key, this.news, this.provider}) : super(key: key);

  @override
  _NewsDetailInfoState createState() => _NewsDetailInfoState();
}

class _NewsDetailInfoState extends State<NewsDetailInfo> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  HtmlEditorController controller = HtmlEditorController();
  String newImageUrl = '';

  @override
  Widget build(BuildContext context) {
    final _width = ScreenQueries.instance.width(context);
    var options = ['Dành cho thợ', 'Dành cho khách'];

    News news = widget.news;

    bool flagInsert;

    if (news == null) {
      flagInsert = true;
      news = new News();
    } else {
      flagInsert = false;
    }

    void doSaveOrUpdate() async {
      _formKey.currentState.save();

      if (flagInsert) {
        // insert
        widget.provider.insertNews(context, news);
      } else {
        // modify
        widget.provider.updateNews(context, news);
      }
      Navigator.of(context).pop();
      widget.provider.fetchData();
    }

    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          width: _width * 0.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: CustomText(
                        text: news != null
                            ? "Cập nhật thông tin"
                            : "Thêm mới thông tin",
                        size: 17,
                        color: Colors.teal,
                        weight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    child: CloseButton(
                      color: Colors.teal,
                    ),
                  )
                ],
              ),
              Divider(indent: 8.0),
              // begin
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 12, 8, 12),
                child: Row(
                  children: [
                    Column(
                      children: [
                        StreamBuilder<String>(
                          stream: downloadUrl(news.thumbnail).asStream(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            } else if (snapshot.connectionState ==
                                ConnectionState.none) {
                              return Image.asset(
                                'default_image.png',
                                width: 150,
                                height: 150,
                                fit: BoxFit.fill,
                              );
                            }

                            String imgUrl = snapshot.data;

                            if (newImageUrl.isEmpty && imgUrl == null) {
                              return Image.asset(
                                'default_image.png',
                                width: 150,
                                height: 150,
                                fit: BoxFit.fill,
                              );
                            }

                            return CommonUtil.loadImage(
                                context,
                                imgUrl != null ? imgUrl : newImageUrl,
                                150,
                                150);
                          },
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                                width: 70,
                                alignment: Alignment.centerLeft,
                                child: IconButton(
                                    onPressed: () {
                                      uploadToStorage(news);
                                    },
                                    icon: Icon(
                                      Icons.add_a_photo,
                                      color: Colors.green,
                                    ))),
                            Container(
                                width: 70,
                                alignment: Alignment.centerRight,
                                child: IconButton(
                                    onPressed: () {},
                                    icon: Icon(
                                      Icons.remove_circle,
                                      color: Colors.red,
                                    ))),
                          ],
                        )
                      ],
                    ),
                    Expanded(
                        child: Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: TextFormField(
                        initialValue: widget.news == null
                            ? '«ID sẽ được tự động tạo»'
                            : widget.news.documentID,
                        decoration: InputDecoration(
                          labelText: 'ID',
                          enabled: !(news != null),
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.check_circle,
                          ),
                        ),
                      ),
                    )),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                child: TextFormField(
                  initialValue: widget.news == null ? '' : widget.news.title,
                  onSaved: (newValue) => news.title = newValue,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Tiêu đề không được rỗng';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    icon: Icon(FontAwesome5Solid.heading),
                    labelText: 'Tiêu đề',
                    border: OutlineInputBorder(),
                    suffixIcon: Icon(
                      Icons.check_circle,
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: CustomText(
                      text: 'Dành cho : ',
                      weight: FontWeight.normal,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: DropDownButton(
                      list: options,
                      selected: (news.isForWorker != null && news.isForWorker)
                          ? options[0]
                          : options[1],
                      onChanged: (value) {
                        news.isForWorker =
                            (options.indexOf(value) == 0 ? true : false);
                      },
                    ),
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: HtmlEditor(
                  controller: controller,
                  htmlToolbarOptions: HtmlToolbarOptions(
                    renderBorder: true,
                    toolbarType: ToolbarType.nativeScrollable,
                  ),
                  htmlEditorOptions: HtmlEditorOptions(
                    initialText: news.content,
                    hint: "Nhập nội dung ở đây..",
                  ),
                  otherOptions: OtherOptions(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                    ),
                    height: 150,
                  ),
                  callbacks: Callbacks(
                    onChange: (value) {
                      news.content = value;
                    },
                  ),
                ),
              ),

              // end
              Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 10),
                height: 50,
                child: ElevatedButton.icon(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      doSaveOrUpdate();
                    }
                  },
                  icon: Icon(Icons.save, size: 18),
                  label: Text("Lưu thông tin"),
                  style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40),
                      )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void chooseImage({@required Function(File file) onSelected}) {
    InputElement uploadInput = FileUploadInputElement()..accept = 'image/*';
    uploadInput.click();

    uploadInput.onChange.listen((event) {
      final file = uploadInput.files.first;
      final reader = FileReader();

      reader.readAsDataUrl(file);
      reader.onLoadEnd.listen((event) {
        onSelected(file);
      });
    });
  }

  void uploadToStorage(News news) {
    chooseImage(
      onSelected: (file) {
        String path = CommonUtil.generatefilePath(COLLECTION_NEWS, file.name);
        storage.refFromURL(STORAGE_BUCKET).child(path).putBlob(file).then((_) {
          _.ref.getDownloadURL().then((value) {
            db
                .collection(COLLECTION_NEWS)
                .doc(news.documentID)
                .update({'thumbnail': value}).then((value1) {
              this.setState(() {
                news.thumbnail = value;
              });
            }).onError((error, stackTrace) {
              this.setState(() {
                news.thumbnail = value;
                newImageUrl = value;
              });
            });
          });
        });
      },
    );
  }

  Future<String> downloadUrl(String photoUrl) {
    if (photoUrl != null && photoUrl.contains(STORAGE_BUCKET)) {
      return storage.ref(photoUrl).getDownloadURL();
    }
    return storage.refFromURL(STORAGE_BUCKET).child(photoUrl).getDownloadURL();
  }
}
