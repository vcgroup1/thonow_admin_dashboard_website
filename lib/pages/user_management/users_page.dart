import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:provider/provider.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_notifier/user_data_notifier.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_table_source/user_data_table_source.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/user.dart';
import 'package:thonow_admin_dashboard_website/utilities/screen_size.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_dialog.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';
import 'package:thonow_admin_dashboard_website/widgets/data_table/custom_paginated_datatable.dart';
import 'package:thonow_admin_dashboard_website/widgets/search_bar/animated_search_bar.dart';

class UsersPage extends StatefulWidget {
  const UsersPage({Key key}) : super(key: key);

  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<UserDataNotifier>(
      create: (context) => UserDataNotifier(),
      child: Padding(
        padding: EdgeInsets.all(20),
        child: _InternalWidget(),
      ),
    );
  }
}

class _InternalWidget extends StatelessWidget {
  const _InternalWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<UserDataNotifier>();
    final _userList = _provider.userList;

    if (_userList.isEmpty) {
      return const SizedBox.shrink();
    }

    final _dataSource = UserDataTableSource(
      context: context,
      data: _userList,
      onRowSelect: (index) =>
          _showDetails(context, _userList[index], _provider),
    );

    AlertDialog setActiveForMultipleUsers(
        BuildContext context, List<User> _selectedUser, bool isActive) {
      return AlertDialog(
        title: Text('Xoá dữ liệu?'),
        content: Text('Bạn có chắc chắn muốn thực hiện tác vụ trên ' +
            _selectedUser.length.toString() +
            ' người dùng này chứ?'),
        actions: [
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Hủy'),
          ),
          TextButton(
            style: TextButton.styleFrom(
              textStyle: TextStyle(color: Colors.teal),
            ),
            onPressed: () async {
              _provider.setActiveForMultipleUser(_selectedUser, isActive);
              Navigator.of(context).pop();
              _provider.fetchData();
            },
            child: Text('Tiếp tục'),
          ),
        ],
      );
    }

    void searchByText(String query) {
      _provider.search(context, query);
    }

    return CustomPaginatedTable(
      dataColumns: _colGen(_dataSource, _provider),
      header: const Text(DataTableConstants.header_users),
      showActions: true,
      source: _dataSource,
      sortColumnIndex: _provider.sortColumnIndex,
      sortColumnAsc: _provider.sortAsc,
      onRowChanged: (index) => _provider.rowsPerPage = index,
      actions: <Widget>[
        AnimatedSearchBar(
          onChanged: (value) => searchByText(value),
        ),
        IconButton(
          splashColor: Colors.transparent,
          icon: const Icon(
            Icons.refresh,
            color: Colors.teal,
          ),
          onPressed: () {
            _provider.fetchData();
            _showSnackbar(context, "Dữ liệu đã được cập nhật!");
          },
        ),
        ElevatedButton.icon(
          onPressed: () {
            print('Thêm mới');
          },
          icon: Icon(Icons.add, size: 18),
          label: Text("Thêm mới"),
          style: ElevatedButton.styleFrom(
            primary: Colors.green,
          ),
        ),
        ElevatedButton.icon(
          onPressed: () {
            List<User> _selectedUser = _dataSource.getSelectedObjects;

            if (_selectedUser.length > 0) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return setActiveForMultipleUsers(
                        context, _selectedUser, false);
                  });
            }
          },
          icon: Icon(
            Icons.lock,
            size: 18,
            color: Colors.white,
          ),
          label: Text("Khoá tài khoản"),
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
          ),
        ),
        ElevatedButton.icon(
          onPressed: () {
            List<User> _selectedUser = _dataSource.getSelectedObjects;

            if (_selectedUser.length > 0) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return setActiveForMultipleUsers(
                        context, _selectedUser, true);
                  });
            }
          },
          icon: Icon(
            Icons.lock_open,
            size: 18,
            color: Colors.white,
          ),
          label: Text("Mở Khoá tài khoản"),
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
          ),
        )
      ],
      rowsPerPage: _provider.rowsPerPage,
    );
  }

  void _showDetails(
      BuildContext context, User user, UserDataNotifier provider) async {
    await showDialog<bool>(
      context: context,
      builder: (_) => CustomDialog(
        showPadding: false,
        child: UserDetailInfo(
          user: user,
          provider: provider,
        ),
      ),
    );
  }

  List<DataColumn> _colGen(
      UserDataTableSource _src, UserDataNotifier _provider) {
    return <DataColumn>[
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_user_uid,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_user_uid,
        onSort: (columnIndex, ascending) {
          _sort<String>(
              (user) => user.uid, columnIndex, ascending, _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_user_displayName,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_user_displayName,
        onSort: (columnIndex, ascending) {
          _sort<String>((user) => user.displayName, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_user_phone,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: DataTableConstants.col_user_phone,
        onSort: (columnIndex, ascending) {
          _sort<String>((user) => user.phoneNumber, columnIndex, ascending,
              _src, _provider);
        },
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_user_status,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: "Trạng thái",
      ),
      DataColumn(
        label: CustomText(
          text: DataTableConstants.col_action,
          color: Colors.teal,
          size: 15,
          weight: FontWeight.bold,
        ),
        tooltip: "Tác vụ",
      )
    ];
  }

  void _sort<T>(Comparable<T> Function(User user) getField, int colIndex,
      bool asc, UserDataTableSource _src, UserDataNotifier _provider) {
    _src.sort<T>(getField, asc);
    _provider.sortAsc = asc;
    _provider.sortColumnIndex = colIndex;
  }

  void _showSnackbar(BuildContext context, String s) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(s),
      duration: const Duration(milliseconds: 2000),
    ));
  }
}

class UserDetailInfo extends StatefulWidget {
  final User user;
  final UserDataNotifier provider;

  const UserDetailInfo({Key key, @required this.user, this.provider})
      : assert(user != null),
        super(key: key);

  @override
  _UserDetailInfoState createState() => _UserDetailInfoState();
}

class _UserDetailInfoState extends State<UserDetailInfo> {
  @override
  Widget build(BuildContext context) {
    final _width = ScreenQueries.instance.width(context);
    bool isActive = widget.user.isActive;

    handleSwitch(bool value) {
      setState(() {
        isActive = value;
      });
    }

    return Container(
      width: _width * 0.4,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: CustomText(
                    text: "Cập nhật thông tin",
                    size: 17,
                    color: Colors.teal,
                    weight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                child: CloseButton(
                  color: Colors.teal,
                ),
              )
            ],
          ),
          Divider(indent: 8.0),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              initialValue: widget.user.uid,
              decoration: InputDecoration(
                labelText: 'UID',
                enabled: false,
                border: OutlineInputBorder(),
                suffixIcon: Icon(
                  Icons.check_circle,
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              initialValue: widget.user.displayName,
              decoration: InputDecoration(
                icon: Icon(FontAwesome5Solid.user_circle),
                labelText: 'Tên hiển thị',
                enabled: false,
                border: OutlineInputBorder(),
                suffixIcon: Icon(
                  Icons.check_circle,
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              initialValue: widget.user.phoneNumber.substring(3),
              decoration: InputDecoration(
                icon: Icon(Icons.phone),
                border: OutlineInputBorder(),
                labelText: 'Số điện thoại',
                enabled: false,
                prefixText: widget.user.phoneNumber.substring(0, 3),
                suffixIcon: Icon(
                  Icons.check_circle,
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CustomText(
                text: 'Trạng thái: ',
                weight: FontWeight.normal,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Switch(
                  value: widget.user.isActive,
                  onChanged: (value) => handleSwitch(value),
                ),
              )
            ],
          ),
          Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 20),
            height: 60,
            child: ElevatedButton.icon(
              onPressed: () {
                print('Save obj');
                widget.provider.setActiveForUser(widget.user, isActive);
                widget.provider.fetchData();
              },
              icon: Icon(Icons.save, size: 18),
              label: Text("Lưu thông tin"),
              style: ElevatedButton.styleFrom(
                  primary: Colors.green,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40),
                  )),
            ),
          )
        ],
      ),
    );
  }
}
