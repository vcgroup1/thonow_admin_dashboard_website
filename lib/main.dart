import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/locator.dart';
import 'package:thonow_admin_dashboard_website/pages/login/login.dart';
import 'package:thonow_admin_dashboard_website/providers/app_provider.dart';
import 'package:thonow_admin_dashboard_website/providers/auth.dart';
import 'package:thonow_admin_dashboard_website/providers/tables.dart';
import 'package:thonow_admin_dashboard_website/rounting/route_names.dart';
import 'package:thonow_admin_dashboard_website/rounting/router.dart';
import 'package:thonow_admin_dashboard_website/widgets/loading.dart';
import 'package:google_fonts/google_fonts.dart';

import 'widgets/layouts/layout.dart';

// void main() {
//   setupLocator();
//   runApp(MultiProvider(providers: [
//     ChangeNotifierProvider.value(value: AppProvider.init()),
//     ChangeNotifierProvider.value(value: AuthProvider.initialize()),
//     ChangeNotifierProvider.value(value: TablesProvider.init())
//   ], child: MyApp()));
// }

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       title: "ThoNOW Admin Dashboard",
//       theme: ThemeData(
//         primarySwatch: Colors.teal,
//       ),
//       onGenerateRoute: generateRoute,
//       initialRoute: PageControllerRoute,
//     );
//   }
// }

class AppPagesController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    return FutureBuilder(
        // init FlutterFire
        future: initialization,
        builder: (context, snapshot) {
          // check for errors
          if (snapshot.hasError) {
            return Scaffold(
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Text("Có gì đó không đúng")],
              ),
            );
          }

          // Once complete, show your application
          if (snapshot.connectionState == ConnectionState.done) {
            print(authProvider.status.toString());
            switch (authProvider.status) {
              case Status.Uninitialized:
                return Loading();
              case Status.Unauthenticated:
              case Status.Authenticating:
                return LoginPage();
              case Status.Authenticated:
                return LayoutTemplate();
              default:
                return LoginPage();
            }
          }

          // Otherwise, show sth while waiting for initialization to complete
          return Scaffold(
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [CircularProgressIndicator()],
            ),
          );
        });
  }
}

Future<void> main() async {
  setupLocator();
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider.value(value: AppProvider.init()),
    ChangeNotifierProvider.value(value: AuthProvider.initialize()),
    ChangeNotifierProvider.value(value: TablesProvider.init())
  ], child: MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    //
    return Builder(
      builder: (BuildContext context) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: "ThoNOW Admin Dashboard",
          theme: ThemeData(
            fontFamily: 'Inter',
            primarySwatch: Colors.teal,
            textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
                .apply(bodyColor: Colors.black),
          ),
          onGenerateRoute: generateRoute,
          initialRoute: PageControllerRoute,
        );
      },
    );
  }
}
