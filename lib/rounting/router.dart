import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/pages/home/home_page.dart';
import 'package:thonow_admin_dashboard_website/pages/login/login.dart';
import 'package:thonow_admin_dashboard_website/pages/news_management/news_page.dart';
import 'package:thonow_admin_dashboard_website/pages/offer_management/offers_page.dart';
import 'package:thonow_admin_dashboard_website/pages/order_management/orders_page.dart';
import 'package:thonow_admin_dashboard_website/pages/user_management/users_page.dart';
import 'package:thonow_admin_dashboard_website/pages/worker_management/workers_page.dart';
import 'package:thonow_admin_dashboard_website/widgets/layouts/layout.dart';

import '../main.dart';
import 'route_names.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  print('generateRoute: ${settings.name}');
  switch (settings.name) {
    case HomeRoute:
      return _NoAnimationMaterialPageRoute(
        builder: (context) => HomePage(),
        name: HomeRoute,
      );
    case WorkersRoute:
      return _NoAnimationMaterialPageRoute(
        builder: (context) => WorkersPage(),
        name: WorkersRoute,
      );
    case OrdersRoute:
      return _NoAnimationMaterialPageRoute(
        builder: (context) => OrdersPage(),
        name: OrdersRoute,
      );
    case OffersRoute:
      return _NoAnimationMaterialPageRoute(
        builder: (context) => OffersPage(),
        name: OffersRoute,
      );
    case NewsRoute:
      return _NoAnimationMaterialPageRoute(
        builder: (context) => NewsPage(),
        name: NewsRoute,
      );
    case UsersRoute:
      return _NoAnimationMaterialPageRoute<dynamic>(
        builder: (context) => UsersPage(),
        name: UsersRoute,
      );
    case LayoutRoute:
      return _NoAnimationMaterialPageRoute<dynamic>(
        builder: (context) => LayoutTemplate(),
        name: LayoutRoute,
      );
    case PageControllerRoute:
      return _NoAnimationMaterialPageRoute<dynamic>(
        builder: (context) => AppPagesController(),
        name: PageControllerRoute,
      );
    case LoginRoute:
    default:
      return _NoAnimationMaterialPageRoute<dynamic>(
        builder: (context) => LoginPage(),
        name: LoginRoute,
      );
  }
}

class _NoAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  _NoAnimationMaterialPageRoute({
    @required WidgetBuilder builder,
    @required String name,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(
          builder: builder,
          maintainState: maintainState,
          settings: RouteSettings(name: name),
          fullscreenDialog: fullscreenDialog,
        );

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation, Widget child) =>
      child;
}

class UndefinedView extends StatelessWidget {
  const UndefinedView({Key key, this.name}) : super(key: key);

  /// Name of the route....
  final String name;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Something went wrong for $name'),
      ),
    );
  }
}
