import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/user.dart';

class UserServices {
  static void updateUserData(Map<String, dynamic> values) {
    db.collection(COLLECTION_USER).doc(values['uid']).update(values);
  }

  static Future<User> getAdminByUid(String uid) =>
      db.collection(COLLECTION_ADMIN).doc(uid).get().then((value) {
        return User.fromFirestore(value);
      });

  static Future<List<User>> getAllUsers() async =>
      db.collection(COLLECTION_USER).get().then((value) {
        List<User> users = [];
        for (DocumentSnapshot user in value.docs) {
          users.add(User.fromFirestore(user));
        }
        return users;
      });

  static Future<void> deleteUser(String uid) async =>
      db.collection(COLLECTION_USER).doc(uid).delete();

  static Future<void> setDisableOrActiveUser(String uid, bool isActive) async {
    Map<String, dynamic> values = Map();
    values.putIfAbsent("isActive", () => isActive);
    db.collection(COLLECTION_USER).doc(uid).update(values);
  }
}
