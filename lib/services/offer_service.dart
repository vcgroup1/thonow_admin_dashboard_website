import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/offer.dart';

class OfferService {
  static Future<Offer> getOffer(String id) async {
    return db
        .collection(COLLECTION_OFFER)
        .doc(id)
        .get()
        .then((value) => Offer.fromFirestore(value));
  }

  static Future<List<Offer>> getAllOffer() async =>
      db.collection(COLLECTION_OFFER).get().then((value) {
        List<Offer> offerList = [];
        value.docs.forEach((element) {
          offerList.add(Offer.fromFirestore(element));
        });
        return offerList;
      });

  static Future<bool> insertOffer(Offer offer) async {
    db
        .collection(COLLECTION_OFFER)
        .add(offer.toMap())
        .whenComplete(() => Future<bool>.value(true))
        .onError(
            (error, stackTrace) => Future.error('Thêm tin tức mới xảy ra lỗi'));
    return Future<bool>.value(true);
  }

  static Future<void> updateOffer(Offer offer) async {
    db.collection(COLLECTION_OFFER).doc(offer.documentID).set(offer.toMap());
  }

  static Future<void> deleteOffer(String id) async =>
      db.collection(COLLECTION_OFFER).doc(id).delete();
}
