import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/news.dart';

class NewsService {
  static Future<News> getNews(String id) async {
    return db
        .collection(COLLECTION_NEWS)
        .doc(id)
        .get()
        .then((value) => News.fromFirestore(value));
  }

  static Future<List<News>> getAllNews() async =>
      db.collection(COLLECTION_NEWS).get().then((value) {
        List<News> newsList = [];
        value.docs.forEach((element) {
          newsList.add(News.fromFirestore(element));
        });
        return newsList;
      });

  static Future<bool> insertNews(News news) async {
    db
        .collection(COLLECTION_NEWS)
        .add(news.toMap())
        .whenComplete(() => Future<bool>.value(true))
        .onError(
            (error, stackTrace) => Future.error('Thêm tin tức mới xảy ra lỗi'));
    return Future<bool>.value(true);
  }

  static Future<void> updateNews(News news) async {
    db.collection(COLLECTION_NEWS).doc(news.documentID).set(news.toMap())
        .whenComplete(() => Future<bool>.value(true))
        .onError((error, stackTrace) =>
            Future.error('Cập nhật tin tức mới xảy ra lỗi'));
    return Future<bool>.value(true);
  }

  static Future<void> deleteNews(String id) async =>
      db.collection(COLLECTION_NEWS).doc(id).delete();
}
