import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/order.dart';
import 'package:thonow_admin_dashboard_website/models/order_dto.dart';

class OrderService {
  static Future<Order> get(String id) async {
    return db
        .collection(COLLECTION_ORDER)
        .doc(id)
        .get()
        .then((value) => Order.fromFirestore(value));
  }

  static Future<List<Order>> getAll() async =>
      db.collection(COLLECTION_ORDER).get().then((value) {
        List<Order> list = [];
        value.docs.forEach((element) {
          list.add(Order.fromFirestore(element));
        });
        return list;
      });

  static Future<List<OrderDTO>> fetchData() async =>
      db.collection(COLLECTION_ORDER).get().then((value) {
        List<OrderDTO> list = [];
        value.docs.forEach((element) {
          list.add(OrderDTO.from(Order.fromFirestore(element)));
        });
        return list;
      });

  static Future<bool> insert(Order object) async {
    db
        .collection(COLLECTION_ORDER)
        .add(object.toMap())
        .whenComplete(() => Future<bool>.value(true))
        .onError((error, stackTrace) => Future.error('Thêm mới xảy ra lỗi'));
    return Future<bool>.value(true);
  }

  static Future<void> update(Order object) async {
    db
        .collection(COLLECTION_ORDER)
        .doc(object.documentID)
        .set(object.toMap())
        .whenComplete(() => Future<bool>.value(true))
        .onError((error, stackTrace) => Future.error('Cập nhật xảy ra lỗi'));
    return Future<bool>.value(true);
  }

  static Future<void> delete(String id) async =>
      db.collection(COLLECTION_ORDER).doc(id).delete();
}
