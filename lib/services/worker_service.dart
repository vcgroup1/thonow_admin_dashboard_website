import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/worker.dart';

class WorkerService {
  static Future<Worker> get(String id) async {
    return db
        .collection(COLLECTION_WORKER)
        .doc(id)
        .get()
        .then((value) => Worker.fromFirestore(value));
  }

  static Future<List<Worker>> getAll() async =>
      db.collection(COLLECTION_WORKER).get().then((value) {
        List<Worker> list = [];
        value.docs.forEach((element) {
          list.add(Worker.fromFirestore(element));
        });
        return list;
      });

  static Future<bool> insert(Worker object) async {
    db
        .collection(COLLECTION_WORKER)
        .add(object.toMap())
        .whenComplete(() => Future<bool>.value(true))
        .onError((error, stackTrace) => Future.error('Thêm mới xảy ra lỗi'));
    return Future<bool>.value(true);
  }

  static Future<void> update(Worker object) async {
    db
        .collection(COLLECTION_WORKER)
        .doc(object.documentID)
        .set(object.toMap())
        .whenComplete(() => Future<bool>.value(true))
        .onError((error, stackTrace) => Future.error('Cập nhật xảy ra lỗi'));
    return Future<bool>.value(true);
  }

  static Future<void> delete(String id) async =>
      db.collection(COLLECTION_WORKER).doc(id).delete();

  static Future<void> setDisableOrActiveWorker(String id, bool isActive) async {
    Map<String, dynamic> values = Map();
    values.putIfAbsent("active", () => isActive);
    db.collection(COLLECTION_WORKER).doc(id).update(values);
  }

  static Future<void> setVerfiedForWorker(String id, bool isVerified) async {
    Map<String, dynamic> values = Map();
    values.putIfAbsent("verified", () => isVerified);
    db.collection(COLLECTION_WORKER).doc(id).update(values);
  }
}
