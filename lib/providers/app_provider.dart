import 'package:flutter/cupertino.dart';
import 'package:thonow_admin_dashboard_website/helpers/enumerators.dart';

class AppProvider with ChangeNotifier {
  DisplayedPage currentPage;

  AppProvider.init() {
    changeCurrentPage(DisplayedPage.HOME);
  }

  changeCurrentPage(DisplayedPage newPage) {
    currentPage = newPage;
    notifyListeners();
  }
}
