import 'package:flutter/cupertino.dart';
import 'package:responsive_table/DatatableHeader.dart';

class TablesProvider with ChangeNotifier {
  List<DatatableHeader> usersTableHeader = [
    DatatableHeader(
        text: "UID",
        value: "uid",
        show: true,
        sortable: true,
        textAlign: TextAlign.left),
    DatatableHeader(
        text: "Tên hiển thị",
        value: "displayName",
        show: true,
        sortable: true,
        flex: 2,
        textAlign: TextAlign.left),
    DatatableHeader(
        text: "Địa chỉ Email",
        value: "email",
        show: true,
        sortable: true,
        textAlign: TextAlign.left)
  ];

  TablesProvider.init() {
    _initData();
  }

  _initData() async {}
}
