import 'dart:async';
import 'package:flutter/widgets.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/user.dart' as user_model;
import 'package:thonow_admin_dashboard_website/services/user_service.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

class AuthProvider with ChangeNotifier {
  User _firebaseUser;
  Status _status = Status.Uninitialized;
  user_model.User _user;

  // getter
  user_model.User get user => _user;
  Status get status => _status;
  User get firebaseUser => _firebaseUser;

  // public variables
  final formKey = GlobalKey<FormState>();

  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  AuthProvider.initialize() {
    _setupAuth();
  }

  _setupAuth() async {
    await initialization.then((value) {
      mAuth.authStateChanges().listen(_onStateChanged);
    });
  }

  _onStateChanged(User firebaseUser) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    if (firebaseUser == null) {
      _status = Status.Unauthenticated;
    } else {
      _firebaseUser = firebaseUser;

      await sharedPreferences.setString("id", firebaseUser.uid);

      _user = await UserServices.getAdminByUid(firebaseUser.uid).then((value) {
        _status = Status.Authenticated;
        return value;
      });
    }
    notifyListeners();
  }

  Future<bool> signIn() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      _status = Status.Authenticating;
      notifyListeners();
      await mAuth
          .signInWithEmailAndPassword(
              email: email.text.trim(), password: password.text.trim())
          .then((value) async {
        await sharedPreferences.setString("id", value.user.uid);
      });
      return true;
    } on Exception catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      print(e.toString());
      return false;
    }
  }

  String validateEmail(String value) {
    if (email.text != null) {
      if (value.trim().isEmpty) {
        return "Vui lòng nhập email";
      } else if (!value.trim().contains(RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"))) {
        return "Vui lòng nhập đúng định dạng email";
      }
    }
    return null;
  }

  Future signOut() async {
    mAuth.signOut();
    _status = Status.Unauthenticated;
    notifyListeners();
    return Future.delayed(Duration.zero);
  }

  void clearController() {
    email.text = "";
    password.text = "";
  }
}
