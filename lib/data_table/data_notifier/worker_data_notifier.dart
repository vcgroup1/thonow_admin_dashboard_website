import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/models/worker.dart';
import 'package:thonow_admin_dashboard_website/services/worker_service.dart';

class WorkerDataNotifier with ChangeNotifier {
  WorkerDataNotifier() {
    fetchData();
  }

  var _list = <Worker>[];
  int _sortColumnIndex;
  bool _sortAsc = true;
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;

  List<Worker> get list => this._list;

  int get sortColumnIndex => this._sortColumnIndex;

  set sortColumnIndex(int sortColumnIndex) {
    this._sortColumnIndex = sortColumnIndex;
    notifyListeners();
  }

  bool get sortAsc => this._sortAsc;

  set sortAsc(bool sortAsc) {
    this._sortAsc = sortAsc;
    notifyListeners();
  }

  int get rowsPerPage => this._rowsPerPage;

  set rowsPerPage(int rowsPerPage) {
    this._rowsPerPage = rowsPerPage;
    notifyListeners();
  }

  Future<void> fetchData() async {
    this._list = await WorkerService.getAll();
    notifyListeners();
  }

  Future<void> setActiveForWorker(Worker worker, bool isActive) async {
    await WorkerService.setDisableOrActiveWorker(worker.documentID, isActive);
    notifyListeners();
  }

  Future<void> setActiveForMultipleWorker(
      List<Worker> workers, bool isActive) async {
    int count = 0;
    workers.forEach((element) async {
      if (isActive && element.balanceMoney < 50000 && !element.verified) {
        return;
      }

      await WorkerService.setDisableOrActiveWorker(
          element.documentID, isActive);
      count++;
    });
    notifyListeners();

    if (count != workers.length) {
      return Future.error(
          "Chỉ được phép active thợ có số dư >= 50.000đ và đã xác minh danh tính.");
    }
  }

  Future<void> setVerifiedForMultipleWorker(
      List<Worker> workers, bool isVerified) async {
    workers.forEach((element) async {
      await WorkerService.setVerfiedForWorker(element.documentID, isVerified);
    });
    notifyListeners();
  }

  Future<void> update(BuildContext context, Worker object) async {
    await WorkerService.update(object).whenComplete(() {
      notifyListeners();
    }).onError((error, stackTrace) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Cập nhật xảy ra lỗi!')));
      return Future<bool>.value(false);
    });
  }

  Future<void> search(BuildContext context, String query) async {
    if (query.isEmpty) {
      this._list = await WorkerService.getAll();
    } else {
      List<Worker> filteredList = [];
      this._list.forEach((element) {
        if (element.id.toLowerCase().contains(query.toLowerCase())) {
          filteredList.add(element);
        }
      });

      if (filteredList.isNotEmpty) {
        this._list.clear();
        this._list = filteredList;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Không có kết quả phù hợp với truy vấn của bạn')));
      }
    }
    notifyListeners();
  }
}
