import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/models/news.dart';
import 'package:thonow_admin_dashboard_website/services/news_service.dart';

class NewsDataNotifier with ChangeNotifier {
  NewsDataNotifier() {
    fetchData();
  }

  var _newsList = <News>[];
  int _sortColumnIndex;
  bool _sortAsc = true;
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;

  List<News> get newsList => this._newsList;

  int get sortColumnIndex => this._sortColumnIndex;

  set sortColumnIndex(int sortColumnIndex) {
    this._sortColumnIndex = sortColumnIndex;
    notifyListeners();
  }

  bool get sortAsc => this._sortAsc;

  set sortAsc(bool sortAsc) {
    this._sortAsc = sortAsc;
    notifyListeners();
  }

  int get rowsPerPage => this._rowsPerPage;

  set rowsPerPage(int rowsPerPage) {
    this._rowsPerPage = rowsPerPage;
    notifyListeners();
  }

  Future<void> fetchData() async {
    this._newsList = await NewsService.getAllNews();
    notifyListeners();
  }

  Future<void> deleteNews(String id) async {
    await NewsService.deleteNews(id);
    notifyListeners();
  }

  Future<void> deleteMultipleNews(List<News> newsList) async {
    newsList.forEach((element) async {
      await NewsService.deleteNews(element.documentID);
    });
    notifyListeners();
  }

  Future<void> insertNews(BuildContext context, News news) async {
    if (news != null) {
      news.createdAt = DateTime.now();

      await NewsService.insertNews(news).whenComplete(() {
        notifyListeners();
      }).onError((error, stackTrace) {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text('Thêm tin tức mới xảy ra lỗi!')));
        return Future<bool>.value(false);
      });
    }
  }

  Future<void> updateNews(BuildContext context, News news) async {
    await NewsService.updateNews(news).whenComplete(() {
      notifyListeners();
    }).onError((error, stackTrace) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Cập nhật tin tức mới xảy ra lỗi!')));
      return Future<bool>.value(false);
    });
  }

  Future<void> search(BuildContext context, String query) async {
    if (query.isEmpty) {
      this._newsList = await NewsService.getAllNews();
    } else {
      List<News> filteredNews = [];
      this._newsList.forEach((element) {
        if (element.id.toLowerCase().contains(query.toLowerCase()) ||
            element.title.toLowerCase().contains(query.toLowerCase())) {
          filteredNews.add(element);
        }
      });

      if (filteredNews.isNotEmpty) {
        this._newsList.clear();
        this._newsList = filteredNews;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Không có kết quả phù hợp với truy vấn của bạn')));
      }
    }
    notifyListeners();
  }
}
