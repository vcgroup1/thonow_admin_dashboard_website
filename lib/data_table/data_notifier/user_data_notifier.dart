import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/models/user.dart';
import 'package:thonow_admin_dashboard_website/services/user_service.dart';

class UserDataNotifier with ChangeNotifier {
  UserDataNotifier() {
    fetchData();
  }

  var _userList = <User>[];
  int _sortColumnIndex;
  bool _sortAsc = true;
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;

  List<User> get userList => this._userList;

  int get sortColumnIndex => this._sortColumnIndex;

  set sortColumnIndex(int sortColumnIndex) {
    this._sortColumnIndex = sortColumnIndex;
    notifyListeners();
  }

  bool get sortAsc => this._sortAsc;

  set sortAsc(bool sortAsc) {
    this._sortAsc = sortAsc;
    notifyListeners();
  }

  int get rowsPerPage => this._rowsPerPage;

  set rowsPerPage(int rowsPerPage) {
    this._rowsPerPage = rowsPerPage;
    notifyListeners();
  }

  Future<void> fetchData() async {
    this._userList = await UserServices.getAllUsers();
    notifyListeners();
  }

  Future<void> deleteUser(String uid) async {
    await UserServices.deleteUser(uid);
    notifyListeners();
  }

  Future<void> deleteMultipleUser(List<User> users) async {
    users.forEach((element) async {
      await UserServices.deleteUser(element.uid);
    });
    notifyListeners();
  }

  Future<void> setActiveForUser(User user, bool isActive) async {
    await UserServices.setDisableOrActiveUser(user.uid, isActive);
    notifyListeners();
  }

  Future<void> setActiveForMultipleUser(List<User> users, bool isActive) async {
    users.forEach((element) async {
      await UserServices.setDisableOrActiveUser(element.uid, isActive);
    });
    notifyListeners();
  }

  Future<void> search(BuildContext context, String query) async {
    if (query.isEmpty) {
      this._userList = await UserServices.getAllUsers();
    } else {
      List<User> filteredUsers = [];
      this._userList.forEach((element) {
        if (element.displayName.toLowerCase().contains(query.toLowerCase()) ||
            element.phoneNumber.contains(query) ||
            element.uid.toLowerCase().contains(query.toLowerCase())) {
          filteredUsers.add(element);
        }
      });

      if (filteredUsers.isNotEmpty) {
        this._userList.clear();
        this._userList = filteredUsers;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Không có kết quả phù hợp với truy vấn của bạn')));
      }
    }
    notifyListeners();
  }
}
