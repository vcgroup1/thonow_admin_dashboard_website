import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/models/offer.dart';
import 'package:thonow_admin_dashboard_website/services/offer_service.dart';

class OfferDataNotifier with ChangeNotifier {
  OfferDataNotifier() {
    fetchData();
  }

  var _offerList = <Offer>[];
  int _sortColumnIndex;
  bool _sortAsc = true;
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;

  List<Offer> get list => this._offerList;

  int get sortColumnIndex => this._sortColumnIndex;

  set sortColumnIndex(int sortColumnIndex) {
    this._sortColumnIndex = sortColumnIndex;
    notifyListeners();
  }

  bool get sortAsc => this._sortAsc;

  set sortAsc(bool sortAsc) {
    this._sortAsc = sortAsc;
    notifyListeners();
  }

  int get rowsPerPage => this._rowsPerPage;

  set rowsPerPage(int rowsPerPage) {
    this._rowsPerPage = rowsPerPage;
    notifyListeners();
  }

  Future<void> fetchData() async {
    this._offerList = await OfferService.getAllOffer();
    notifyListeners();
  }

  Future<void> deleteOffer(String id) async {
    await OfferService.deleteOffer(id);
    notifyListeners();
  }

  Future<void> deleteMultipleOffer(List<Offer> newsList) async {
    newsList.forEach((element) async {
      await OfferService.deleteOffer(element.documentID);
    });
    notifyListeners();
  }

  Future<void> insertOffer(BuildContext context, Offer offer) async {
    if (offer != null) {
      offer.createdAt = DateTime.now();

      if (offer.isForWorker == null) offer.isForWorker = true;

      await OfferService.insertOffer(offer).whenComplete(() {
        notifyListeners();
      }).onError((error, stackTrace) {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text('Thêm khuyến mãi mới xảy ra lỗi!')));
        return Future<bool>.value(false);
      });
    }
  }

  Future<void> updateOffer(BuildContext context, Offer offer) async {
    await OfferService.updateOffer(offer).whenComplete(() {
      notifyListeners();
    }).onError((error, stackTrace) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Cập nhật khuyến mãi xảy ra lỗi!')));
      return Future<bool>.value(false);
    });
  }

  Future<void> search(BuildContext context, String query) async {
    if (query.isEmpty) {
      this._offerList = await OfferService.getAllOffer();
    } else {
      List<Offer> filteredOffers = [];
      this._offerList.forEach((element) {
        if (element.id.toLowerCase().contains(query.toLowerCase()) ||
            element.title.toLowerCase().contains(query.toLowerCase())) {
          filteredOffers.add(element);
        }
      });

      if (filteredOffers.isNotEmpty) {
        this._offerList.clear();
        this._offerList = filteredOffers;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Không có kết quả phù hợp với truy vấn của bạn')));
      }
    }
    notifyListeners();
  }
}
