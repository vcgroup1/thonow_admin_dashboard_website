import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/models/order.dart';
import 'package:thonow_admin_dashboard_website/services/order_service.dart';

class OrderDataNotifier with ChangeNotifier {
  OrderDataNotifier() {
    fetchData();
  }

  var _list = <Order>[];
  int _sortColumnIndex;
  bool _sortAsc = true;
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;

  List<Order> get list => this._list;

  int get sortColumnIndex => this._sortColumnIndex;

  set sortColumnIndex(int sortColumnIndex) {
    this._sortColumnIndex = sortColumnIndex;
    notifyListeners();
  }

  bool get sortAsc => this._sortAsc;

  set sortAsc(bool sortAsc) {
    this._sortAsc = sortAsc;
    notifyListeners();
  }

  int get rowsPerPage => this._rowsPerPage;

  set rowsPerPage(int rowsPerPage) {
    this._rowsPerPage = rowsPerPage;
    notifyListeners();
  }

  Future<void> fetchData() async {
    this._list = await OrderService.getAll();
    notifyListeners();
  }

  Future<void> delete(String id) async {
    await OrderService.delete(id);
    notifyListeners();
  }

  Future<void> search(BuildContext context, String query) async {
    if (query.isEmpty) {
      this._list = await OrderService.getAll();
    } else {
      List<Order> filteredList = [];
      this._list.forEach((element) {
        if (element.id.toLowerCase().contains(query.toLowerCase())) {
          filteredList.add(element);
        }
      });

      if (filteredList.isNotEmpty) {
        this._list.clear();
        this._list = filteredList;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Không có kết quả phù hợp với truy vấn của bạn')));
      }
    }
    notifyListeners();
  }

  // filter theo
  // + status
  // + ngày
  //
}
