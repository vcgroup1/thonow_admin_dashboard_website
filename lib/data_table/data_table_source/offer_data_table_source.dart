import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/offer.dart';
import 'package:thonow_admin_dashboard_website/utilities/common_utils.dart';

import 'generic_data_table_source.dart';

class OfferDataTableSource extends GenericDataTableSource<Offer> {
  OfferDataTableSource({
    @required BuildContext context,
    @required List<Offer> data,
    @required OnRowSelect onRowSelect,
  }) : super(
          dtsContext: context,
          data: data,
          onRowSelect: onRowSelect,
        );

  @override
  DataRow getRow(int index) {
    if (index >= datas.length) {
      return null;
    }

    final row = getRowAt(index);

    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(Text('${row.object.documentID}')),
        DataCell(row.object.thumbnail != null
            ? CommonUtil.loadImage(dtsContext, row.object.thumbnail, 40, 40)
            : Image.asset(
                'default_image.png',
                width: 40,
                height: 40,
                fit: BoxFit.fill,
              )),
        DataCell(Text('${row.object.title}')),
        DataCell(Text(simpleDateFormat.format(row.object.createdAt))),
        DataCell(TextButton(
          onPressed: () => onRowSelect(index),
          child: Text("Xem thông tin"),
          style: TextButton.styleFrom(
            primary: Colors.teal,
          ),
        )),
      ],
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          selectedCount += (value ? 1 : -1);
          addOrRemoveSelectedRow(row.object, value);
          assert(selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
    );
  }

  void sort<T>(Comparable<T> Function(Offer u1) getField, bool asc) {
    datas.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);
      return asc
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });
    notifyListeners();
  }
}
