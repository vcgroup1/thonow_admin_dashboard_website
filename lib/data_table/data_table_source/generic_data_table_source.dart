import 'package:flutter/material.dart';

typedef OnRowSelect = void Function(int index);

class GenericDataTableSource<T> extends DataTableSource {
  BuildContext dtsContext;
  List<T> datas;
  OnRowSelect onRowSelect;
  int selectedCount = 0;
  List<Row<T>> rows = [];
  List<T> selectedDatas = [];

  GenericDataTableSource({
    @required BuildContext dtsContext,
    @required List<T> data,
    @required OnRowSelect onRowSelect,
  }) {
    this.datas = data;
    this.dtsContext = dtsContext;
    this.onRowSelect = onRowSelect;
    populateDataInRows();
  }

  List<T> get getSelectedObjects => selectedDatas;

  void populateDataInRows() {
    if (datas != null && datas.isNotEmpty) {
      for (var data in datas) {
        rows.add(Row(data));
      }
    }
  }

  Row<T> getRowAt(int index) {
    return rows[index];
  }

  void addOrRemoveSelectedRow(T _object, bool value) {
    if (value && !selectedDatas.contains(_object)) {
      selectedDatas.add(_object);
    } else {
      selectedDatas.remove(_object);
    }
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => datas == null ? 0 : datas.length;

  @override
  int get selectedRowCount => selectedCount;

  @override
  DataRow getRow(int index) {
    throw UnimplementedError();
  }
}

class Row<T> {
  final T object;
  bool selected = false;

  Row(
    this.object,
  );

  @override
  String toString() {
    return '${object.toString()}, ${selected.toString()}';
  }
}
