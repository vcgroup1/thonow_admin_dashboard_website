import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_table_source/generic_data_table_source.dart'
    as generic;
import 'package:thonow_admin_dashboard_website/models/worker.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';

typedef OnShowBankAccountInfo = void Function(int index);

class WorkerDataTableSource extends generic.GenericDataTableSource<Worker> {
  final OnShowBankAccountInfo _onShowBankAccountInfo;

  WorkerDataTableSource({
    @required BuildContext context,
    @required List<Worker> data,
    @required generic.OnRowSelect onRowSelect,
    @required OnShowBankAccountInfo onShowBankAccountInfo,
  })  : _onShowBankAccountInfo = onShowBankAccountInfo,
        super(
          dtsContext: context,
          data: data,
          onRowSelect: onRowSelect,
        );

  @override
  DataRow getRow(int index) {
    if (index >= datas.length) {
      return null;
    }

    generic.Row row = getRowAt(index);

    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(Text('${row.object.documentID}')),
        DataCell(Text('${row.object.displayName}')),
        DataCell(Text('${row.object.phoneNumber}')),
        DataCell(
          Row(
            children: [
              Icon(
                Icons.verified_user_rounded,
                color: row.object.verified ? Colors.green : Colors.grey[400],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: CustomText(
                  text: row.object.verified ? 'Đã xác minh' : 'Chưa xác minh',
                  color: row.object.verified ? Colors.green : Colors.grey[400],
                  size: 15,
                ),
              ),
            ],
          ),
        ),
        DataCell(Row(
          children: [
            TextButton(
              onPressed: () => onRowSelect(index),
              child: row.object.active
                  ? Icon(
                      Icons.circle,
                      color: Colors.green,
                      size: 18,
                    )
                  : Icon(
                      Icons.circle,
                      color: Colors.red,
                      size: 18,
                    ),
              style: TextButton.styleFrom(
                primary: Colors.teal,
              ),
            ),
            row.object.active
                ? CustomText(
                    text: 'Đang hoạt động',
                    color: Colors.green,
                    weight: FontWeight.w500,
                  )
                : CustomText(
                    text: 'Đang bị khoá',
                    color: Colors.red,
                    weight: FontWeight.w500,
                  ),
          ],
        )),
        DataCell(TextButton(
          onPressed: () => this.onRowSelect(index),
          child: Text("Xem thông tin"),
          style: TextButton.styleFrom(
            primary: Colors.teal,
          ),
        )),
        DataCell(
          TextButton(
            onPressed: () => this._onShowBankAccountInfo(index),
            child: Text("Xem thông tin thẻ ngân hàng"),
            style: TextButton.styleFrom(
              primary: Colors.blue,
            ),
          ),
        ),
      ],
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          selectedCount += (value ? 1 : -1);
          print(selectedCount.toString());
          addOrRemoveSelectedRow(row.object, value);
          assert(selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
    );
  }

  void sort<T>(Comparable<T> Function(Worker u1) getField, bool asc) {
    datas.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);
      return asc
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });
    notifyListeners();
  }
}
