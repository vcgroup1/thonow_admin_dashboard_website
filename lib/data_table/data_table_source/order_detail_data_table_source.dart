import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/models/orderdetail.dart';
import 'package:thonow_admin_dashboard_website/models/service.dart';

import 'generic_data_table_source.dart';

class OrderDetailDataTableSource extends GenericDataTableSource<OrderDetail> {
  OrderDetailDataTableSource({
    @required BuildContext context,
    @required List<OrderDetail> data,
    @required OnRowSelect onRowSelect,
  }) : super(
          dtsContext: context,
          data: data,
          onRowSelect: onRowSelect,
        );

  @override
  DataRow getRow(int index) {
    if (index >= datas.length) {
      return null;
    }

    final row = getRowAt(index);

    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(Text(index.toString())),
        DataCell(StreamBuilder<DocumentSnapshot>(
          stream: getObject(row.object.service),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }

            Service service = Service.fromFirestore(snapshot.data);

            return Text(service.name);
          },
        )),
        DataCell(Text(row.object.quantity.toString())),
        DataCell(Text(row.object.unitPrice.toString())),
        DataCell(Text(row.object.subtotal.toString())),
      ],
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          selectedCount += (value ? 1 : -1);
          addOrRemoveSelectedRow(row.object, value);
          assert(selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
    );
  }

  Stream<DocumentSnapshot> getObject(DocumentReference docRef) {
    return docRef.snapshots();
  }
}
