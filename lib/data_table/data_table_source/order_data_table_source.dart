import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_table_source/generic_data_table_source.dart'
    as generic;
import 'package:thonow_admin_dashboard_website/helpers/constants.dart';
import 'package:thonow_admin_dashboard_website/models/order.dart';
import 'package:thonow_admin_dashboard_website/models/orderdetail.dart';
import 'package:thonow_admin_dashboard_website/models/service.dart';
import 'package:thonow_admin_dashboard_website/models/user.dart';
import 'package:thonow_admin_dashboard_website/models/worker.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';
import 'package:thonow_admin_dashboard_website/models/orderstatus.dart';

typedef OnRowSelect = void Function(int index);

class OrderDataTableSource extends generic.GenericDataTableSource<Order> {
  List<generic.Row<Order>> _rows = [];

  OrderDataTableSource({
    @required BuildContext context,
    @required List<Order> data,
    @required OnRowSelect onRowSelect,
  }) : super(dtsContext: context, data: data, onRowSelect: onRowSelect);

  @override
  DataRow getRow(int index) {
    if (index >= datas.length) {
      return null;
    }

    final row = getRowAt(index);

    // if (row.object == null) {
    //   return null;
    // }

    if (index == datas.length) {
      notifyListeners();
    }

    var dataRow;
    try {
      dataRow = DataRow.byIndex(
        index: index,
        cells: <DataCell>[
          DataCell(Text(row.object.id)),
          DataCell(StreamBuilder<QuerySnapshot>(
            stream: getServices(row.object.orderDetails),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              if (snapshot.hasError) {
                return Text("Something went wrong");
              }

              List<Widget> serviceList = [];
              snapshot.data.docs.forEach((element) {
                Service service = Service.fromFirestore(element);
                serviceList.add(Text(service.name));
              });

              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: serviceList,
              );
            },
          )),
          DataCell(StreamBuilder<DocumentSnapshot>(
            stream: getObject(row.object.customer),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              User customer = User.fromFirestore(snapshot.data);
              return Text(customer != null ? customer.displayName : '');
            },
          )),
          DataCell(row.object.worker == null
              ? Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  height: double.infinity,
                  color: Colors.yellow,
                  child: CustomText(
                    text: 'Chưa có',
                    color: Colors.black,
                  ),
                )
              : StreamBuilder<DocumentSnapshot>(
                  stream: getObject(row.object.worker),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    Worker worker = Worker.fromFirestore(snapshot.data);

                    return Text(worker.displayName);
                  },
                )),
          DataCell(
            Chip(
              label: CustomText(
                text: row.object.status.title,
                color: Colors.white,
                size: 14,
              ),
              avatar: Icon(
                FontAwesome5Solid.box,
                size: 14,
                color: Colors.white,
              ),
              backgroundColor: HexColor(row.object.status.color),
            ),
          ),
          DataCell(Text(orderDateFormat.format(row.object.workingDate))),
          DataCell(Text(simpleCurrencyFormat.format(row.object.total == 0
              ? row.object.estimatedFee
              : row.object.total))),
          DataCell(
            TextButton(
              onPressed: () => onRowSelect(index),
              child: Text("Xem thông tin"),
              style: TextButton.styleFrom(
                primary: Colors.teal,
              ),
            ),
          ),
        ],
        selected: row.selected,
        onSelectChanged: (value) {
          if (row.selected != value) {
            selectedCount += (value ? 1 : -1);
            print(selectedCount.toString());
            addOrRemoveSelectedRow(row.object, value);
            assert(selectedCount >= 0);
            row.selected = value;
            notifyListeners();
          }
        },
      );
    } on Exception catch (e) {
      print(e.toString());
    }
    return dataRow;
  }

  Stream<QuerySnapshot> getServices(List<OrderDetail> orderDetails) {
    List<String> serviceIdList = [];

    orderDetails.forEach((element) {
      serviceIdList.add(element.service.id);
    });

    return db
        .collection(COLLECTION_SERVICE)
        .where('id', whereIn: serviceIdList)
        .snapshots();
  }

  Stream<DocumentSnapshot> getObject(DocumentReference docRef) {
    return docRef.snapshots();
  }

  void sort<T>(Comparable<T> Function(Order u1) getField, bool asc) {
    datas.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);
      return asc
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });
    notifyListeners();
  }
}
