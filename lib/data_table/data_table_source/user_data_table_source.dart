import 'package:flutter/material.dart';
import 'package:thonow_admin_dashboard_website/data_table/data_table_source/generic_data_table_source.dart'
    as gdtb;
import 'package:thonow_admin_dashboard_website/models/user.dart';
import 'package:thonow_admin_dashboard_website/widgets/custom_text.dart';

typedef OnRowSelect = void Function(int index);

class UserDataTableSource extends gdtb.GenericDataTableSource<User> {
  UserDataTableSource({
    @required BuildContext context,
    @required List<User> data,
    @required OnRowSelect onRowSelect,
  }) : super(
          dtsContext: context,
          data: data,
          onRowSelect: onRowSelect,
        );

  @override
  DataRow getRow(int index) {
    assert(index >= 0);

    if (index >= datas.length) {
      return null;
    }

    final row = getRowAt(index);

    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(Text('${row.object.uid}')),
        DataCell(Text('${row.object.displayName}')),
        DataCell(Text('${row.object.phoneNumber}')),
        DataCell(Row(
          children: [
            TextButton(
              onPressed: () => onRowSelect(index),
              child: row.object.isActive
                  ? Icon(
                      Icons.circle,
                      color: Colors.green,
                      size: 18,
                    )
                  : Icon(
                      Icons.circle,
                      color: Colors.red,
                      size: 18,
                    ),
              style: TextButton.styleFrom(
                primary: Colors.teal,
              ),
            ),
            row.object.isActive
                ? CustomText(
                    text: 'Đang hoạt động',
                    color: Colors.green,
                    weight: FontWeight.w500,
                  )
                : CustomText(
                    text: 'Đang bị khoá',
                    color: Colors.red,
                    weight: FontWeight.w500,
                  ),
          ],
        )),
        DataCell(TextButton(
          onPressed: () => onRowSelect(index),
          child: Text("Xem thông tin"),
          style: TextButton.styleFrom(
            primary: Colors.teal,
          ),
        )),
      ],
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          selectedCount += (value ? 1 : -1);
          print(selectedCount.toString());
          addOrRemoveSelectedRow(row.object, value);
          assert(selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
    );
  }

  void sort<T>(Comparable<T> Function(User u1) getField, bool asc) {
    datas.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);
      return asc
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });
    notifyListeners();
  }
}
