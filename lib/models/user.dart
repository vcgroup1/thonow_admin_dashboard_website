import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  final String uid;
  final String displayName;
  final String phoneNumber;
  final String email;
  final String photoUrl;
  final List<DocumentReference> favoriteWorkerList;
  final bool isActive;
  final DocumentSnapshot snapshot;
  final DocumentReference reference;
  final String documentID;

  User({
    this.uid,
    this.displayName,
    this.phoneNumber,
    this.email,
    this.photoUrl,
    this.favoriteWorkerList,
    this.isActive,
    this.snapshot,
    this.reference,
    this.documentID,
  });

  factory User.fromFirestore(DocumentSnapshot snapshot) {
    if (snapshot == null) return null;
    var map = snapshot.data();

    return User(
      uid: map['uid'],
      displayName: map['displayName'],
      phoneNumber: map['phoneNumber'],
      email: map['email'],
      photoUrl: map['photoUrl'],
      favoriteWorkerList: map['favoriteWorkerList'] != null
          ? List<DocumentReference>.from(map['favoriteWorkerList'])
          : null,
      isActive: map['isActive'],
      snapshot: snapshot,
      reference: snapshot.reference,
      documentID: snapshot.id,
    );
  }

  factory User.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return User(
      uid: map['uid'],
      displayName: map['displayName'],
      phoneNumber: map['phoneNumber'],
      email: map['email'],
      photoUrl: map['photoUrl'],
      favoriteWorkerList: map['favoriteWorkerList'] != null
          ? List<DocumentReference>.from(map['favoriteWorkerList'])
          : null,
      isActive: map['isActive'],
    );
  }

  Map<String, dynamic> toMap() => {
        'uid': uid,
        'displayName': displayName,
        'phoneNumber': phoneNumber,
        'email': email,
        'photoUrl': photoUrl,
        'favoriteWorkerList': favoriteWorkerList,
        'isActive': isActive,
      };

  User copyWith({
    String uid,
    String displayName,
    String phoneNumber,
    String email,
    String photoUrl,
    List<DocumentReference> favoriteWorkerList,
    bool isActive,
  }) {
    return User(
      uid: uid ?? this.uid,
      displayName: displayName ?? this.displayName,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      email: email ?? this.email,
      photoUrl: photoUrl ?? this.photoUrl,
      favoriteWorkerList: favoriteWorkerList ?? this.favoriteWorkerList,
      isActive: isActive ?? this.isActive,
    );
  }

  @override
  String toString() {
    return '${uid.toString()}, ${displayName.toString()}, ${phoneNumber.toString()}, ${email.toString()}, ${photoUrl.toString()}, ${favoriteWorkerList.toString()}, ${isActive.toString()}, ';
  }

  @override
  bool operator ==(other) => other is User && documentID == other.documentID;

  int get hashCode => documentID.hashCode;
}
