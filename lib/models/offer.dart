import 'package:cloud_firestore/cloud_firestore.dart';

class Offer {
  String id;
  String title;
  String subtitle;
  String content;
  String thumbnail;
  DateTime createdAt;
  String code;
  bool isForWorker;
  DocumentSnapshot snapshot;
  DocumentReference reference;
  String documentID;

  String get getId => this.id;

  set setId(String id) => this.id = id;

  get getTitle => this.title;

  set setTitle(title) => this.title = title;

  get getSubtitle => this.subtitle;

  set setSubtitle(subtitle) => this.subtitle = subtitle;

  get getContent => this.content;

  set setContent(content) => this.content = content;

  get getThumbnail => this.thumbnail;

  set setThumbnail(thumbnail) => this.thumbnail = thumbnail;

  get getCreatedAt => this.createdAt;

  set setCreatedAt(createdAt) => this.createdAt = createdAt;

  get getCode => this.code;

  set setCode(code) => this.code = code;

  get getIsForWorker => this.isForWorker;

  set setIsForWorker(isForWorker) => this.isForWorker = isForWorker;

  get getSnapshot => this.snapshot;

  set setSnapshot(snapshot) => this.snapshot = snapshot;

  get getReference => this.reference;

  set setReference(reference) => this.reference = reference;

  get getDocumentID => this.documentID;

  set setDocumentID(documentID) => this.documentID = documentID;

  Offer({
    this.id,
    this.title,
    this.subtitle,
    this.content,
    this.thumbnail,
    this.createdAt,
    this.code,
    this.isForWorker,
    this.snapshot,
    this.reference,
    this.documentID,
  });

  factory Offer.fromFirestore(DocumentSnapshot snapshot) {
    if (snapshot == null) return null;
    var map = snapshot.data();

    return Offer(
      id: map['id'],
      title: map['title'],
      subtitle: map['subtitle'],
      content: map['content'],
      thumbnail: map['thumbnail'],
      createdAt: map['createdAt']?.toDate(),
      code: map['code'],
      isForWorker: map['isForWorker'],
      snapshot: snapshot,
      reference: snapshot.reference,
      documentID: snapshot.id,
    );
  }

  factory Offer.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Offer(
      id: map['id'],
      title: map['title'],
      subtitle: map['subtitle'],
      content: map['content'],
      thumbnail: map['thumbnail'],
      createdAt: map['createdAt']?.toDate(),
      code: map['code'],
      isForWorker: map['isForWorker'],
    );
  }

  Map<String, dynamic> toMap() => {
        'id': id,
        'title': title,
        'subtitle': subtitle,
        'content': content,
        'thumbnail': thumbnail,
        'createdAt': createdAt,
        'code': code,
        'isForWorker': isForWorker,
      };

  Offer copyWith({
    String id,
    String title,
    String subtitle,
    String content,
    String thumbnail,
    DateTime createdAt,
    String code,
    bool isForWorker,
  }) {
    return Offer(
      id: id ?? this.id,
      title: title ?? this.title,
      subtitle: subtitle ?? this.subtitle,
      content: content ?? this.content,
      thumbnail: thumbnail ?? this.thumbnail,
      createdAt: createdAt ?? this.createdAt,
      code: code ?? this.code,
      isForWorker: isForWorker ?? this.isForWorker,
    );
  }

  @override
  String toString() {
    return '${id.toString()}, ${title.toString()}, ${subtitle.toString()}, ${content.toString()}, ${thumbnail.toString()}, ${createdAt.toString()}, ${code.toString()}, ${isForWorker.toString()}, ';
  }

  @override
  bool operator ==(other) => other is Offer && documentID == other.documentID;

  int get hashCode => documentID.hashCode;
}
