import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:thonow_admin_dashboard_website/helpers/enum_helper_functions.dart';
import 'package:thonow_admin_dashboard_website/models/order.dart';
import 'package:thonow_admin_dashboard_website/models/service.dart';
import 'package:thonow_admin_dashboard_website/models/user.dart';
import 'package:thonow_admin_dashboard_website/models/worker.dart';

import 'address.dart';
import 'feedback.dart';
import 'materialcost.dart';
import 'orderdetail.dart';
import 'orderstatus.dart';
import 'ordertype.dart';

class OrderDTO {
  String id;
  OrderType type;
  DateTime workingDate;
  DateTime finishDate;
  String note;
  double movingExpense;
  double estimatedFee;
  double overtimeFee;
  double otherExpense;
  double tip;
  double total;
  User customer;
  Worker worker;
  List<OrderDetail> orderDetails;
  OrderStatus status;
  Address workingAddress;
  List<MaterialCost> materialCosts;
  Feedback feedback;
  List<String> images;
  Service service;

  get getService => this.service;

  set setService(Service service) => this.service = service;

  String get getId => this.id;

  set setId(String id) => this.id = id;

  get getType => this.type;

  set setType(type) => this.type = type;

  get getWorkingDate => this.workingDate;

  set setWorkingDate(workingDate) => this.workingDate = workingDate;

  get getFinishDate => this.finishDate;

  set setFinishDate(finishDate) => this.finishDate = finishDate;

  get getNote => this.note;

  set setNote(note) => this.note = note;

  get getMovingExpense => this.movingExpense;

  set setMovingExpense(movingExpense) => this.movingExpense = movingExpense;

  get getEstimatedFee => this.estimatedFee;

  set setEstimatedFee(estimatedFee) => this.estimatedFee = estimatedFee;

  get getOvertimeFee => this.overtimeFee;

  set setOvertimeFee(overtimeFee) => this.overtimeFee = overtimeFee;

  get getOtherExpense => this.otherExpense;

  set setOtherExpense(otherExpense) => this.otherExpense = otherExpense;

  get getTip => this.tip;

  set setTip(tip) => this.tip = tip;

  get getTotal => this.total;

  set setTotal(total) => this.total = total;

  get getCustomer => this.customer;

  set setCustomer(customer) => this.customer = customer;

  get getWorker => this.worker;

  set setWorker(worker) => this.worker = worker;

  get getOrderDetails => this.orderDetails;

  set setOrderDetails(orderDetails) => this.orderDetails = orderDetails;

  get getStatus => this.status;

  set setStatus(status) => this.status = status;

  get getWorkingAddress => this.workingAddress;

  set setWorkingAddress(workingAddress) => this.workingAddress = workingAddress;

  get getMaterialCosts => this.materialCosts;

  set setMaterialCosts(materialCosts) => this.materialCosts = materialCosts;

  get getFeedback => this.feedback;

  set setFeedback(feedback) => this.feedback = feedback;

  get getImages => this.images;

  set setImages(images) => this.images = images;
  DocumentSnapshot snapshot;
  DocumentReference reference;
  String documentID;

  OrderDTO({
    this.id,
    this.type,
    this.workingDate,
    this.finishDate,
    this.note,
    this.movingExpense,
    this.estimatedFee,
    this.overtimeFee,
    this.otherExpense,
    this.tip,
    this.total,
    this.customer,
    this.worker,
    this.orderDetails,
    this.status,
    this.workingAddress,
    this.materialCosts,
    this.feedback,
    this.images,
    this.snapshot,
    this.reference,
    this.documentID,
  });

  factory OrderDTO.fromFirestore(DocumentSnapshot snapshot) {
    if (snapshot == null) return null;
    var map = snapshot.data();

    return OrderDTO(
      id: map['id'],
      type: enumFromString(map['type'], OrderType.values),
      workingDate: map['workingDate']?.toDate(),
      finishDate: map['finishDate']?.toDate(),
      note: map['note'],
      movingExpense: map['movingExpense'],
      estimatedFee: map['estimatedFee'],
      overtimeFee: map['overtimeFee'],
      otherExpense: map['otherExpense'],
      tip: map['tip'],
      total: map['total'],
      customer: map['customer'] != null ? User.fromMap(map['customer']) : null,
      worker: map['worker'] != null ? Worker.fromMap(map['worker']) : null,
      orderDetails: map['orderDetails'] != null
          ? List<OrderDetail>.from(map['orderDetails'])
          : null,
      status: enumFromString(map['status'], OrderStatus.values),
      workingAddress: map['workingAddress'] != null
          ? Address.fromMap(map['workingAddress'])
          : null,
      materialCosts: map['materialCosts'] != null
          ? List<MaterialCost>.from(map['materialCosts'])
          : null,
      feedback:
          map['feedback'] != null ? Feedback.fromMap(map['feedback']) : null,
      images: map['images'] != null ? List<String>.from(map['images']) : null,
      snapshot: snapshot,
      reference: snapshot.reference,
      documentID: snapshot.id,
    );
  }

  factory OrderDTO.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return OrderDTO(
      id: map['id'],
      type: enumFromString(map['type'], OrderType.values),
      workingDate: map['workingDate']?.toDate(),
      finishDate: map['finishDate']?.toDate(),
      note: map['note'],
      movingExpense: map['movingExpense'],
      estimatedFee: map['estimatedFee'],
      overtimeFee: map['overtimeFee'],
      otherExpense: map['otherExpense'],
      tip: map['tip'],
      total: map['total'],
      customer: map['customer'] != null ? User.fromMap(map['customer']) : null,
      worker: map['worker'] != null ? Worker.fromMap(map['worker']) : null,
      orderDetails: map['orderDetails'] != null
          ? List<OrderDetail>.from(map['orderDetails'])
          : null,
      status: enumFromString(map['status'], OrderStatus.values),
      workingAddress: map['workingAddress'] != null
          ? Address.fromMap(map['workingAddress'])
          : null,
      materialCosts: map['materialCosts'] != null
          ? List<MaterialCost>.from(map['materialCosts'])
          : null,
      feedback:
          map['feedback'] != null ? Feedback.fromMap(map['feedback']) : null,
      images: map['images'] != null ? List<String>.from(map['images']) : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'id': id,
        'type': type,
        'workingDate': workingDate,
        'finishDate': finishDate,
        'note': note,
        'movingExpense': movingExpense,
        'estimatedFee': estimatedFee,
        'overtimeFee': overtimeFee,
        'otherExpense': otherExpense,
        'tip': tip,
        'total': total,
        'customer': customer,
        'worker': worker,
        'orderDetails': orderDetails,
        'status': status,
        'workingAddress': workingAddress,
        'materialCosts': materialCosts,
        'feedback': feedback,
        'images': images,
      };

  OrderDTO copyWith({
    String id,
    OrderType type,
    DateTime workingDate,
    DateTime finishDate,
    String note,
    double movingExpense,
    double estimatedFee,
    double overtimeFee,
    double otherExpense,
    double tip,
    double total,
    User customer,
    Worker worker,
    List<OrderDetail> orderDetails,
    OrderStatus status,
    Address workingAddress,
    List<MaterialCost> materialCosts,
    Feedback feedback,
    List<String> images,
  }) {
    return OrderDTO(
      id: id ?? this.id,
      type: type ?? this.type,
      workingDate: workingDate ?? this.workingDate,
      finishDate: finishDate ?? this.finishDate,
      note: note ?? this.note,
      movingExpense: movingExpense ?? this.movingExpense,
      estimatedFee: estimatedFee ?? this.estimatedFee,
      overtimeFee: overtimeFee ?? this.overtimeFee,
      otherExpense: otherExpense ?? this.otherExpense,
      tip: tip ?? this.tip,
      total: total ?? this.total,
      customer: customer ?? this.customer,
      worker: worker ?? this.worker,
      orderDetails: orderDetails ?? this.orderDetails,
      status: status ?? this.status,
      workingAddress: workingAddress ?? this.workingAddress,
      materialCosts: materialCosts ?? this.materialCosts,
      feedback: feedback ?? this.feedback,
      images: images ?? this.images,
    );
  }

  @override
  String toString() {
    return '${id.toString()}, ${type.toString()}, ${workingDate.toString()}, ${finishDate.toString()}, ${note.toString()}, ${movingExpense.toString()}, ${estimatedFee.toString()}, ${overtimeFee.toString()}, ${otherExpense.toString()}, ${tip.toString()}, ${total.toString()}, ${customer.toString()}, ${worker.toString()}, ${orderDetails.toString()}, ${status.toString()}, ${workingAddress.toString()}, ${materialCosts.toString()}, ${feedback.toString()}, ${images.toString()}, ';
  }

  @override
  bool operator ==(other) =>
      other is OrderDTO && documentID == other.documentID;

  int get hashCode => documentID.hashCode;

  static OrderDTO from(Order order) {
    return new OrderDTO().copyWith(
      id: order.documentID,
      estimatedFee: order.estimatedFee,
      feedback: order.feedback,
      finishDate: order.finishDate,
      images: order.images,
      materialCosts: order.materialCosts,
      movingExpense: order.movingExpense,
      note: order.note,
      orderDetails: order.orderDetails,
      otherExpense: order.otherExpense,
      overtimeFee: order.overtimeFee,
      status: order.status,
      tip: order.tip,
      total: order.total,
      type: order.type,
      workingAddress: order.workingAddress,
      workingDate: order.workingDate,
    );
  }
}
