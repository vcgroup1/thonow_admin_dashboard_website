import 'package:cloud_firestore/cloud_firestore.dart';

class ServiceType {
  String id;
  String name;
  String drawableName;
  int totalOrders;
  DocumentSnapshot snapshot;
  DocumentReference reference;
  String documentID;

  get getId => this.id;

  set setId(id) => this.id = id;

  get getName => this.name;

  set setName(name) => this.name = name;

  get getDrawableName => this.drawableName;

  set setDrawableName(drawableName) => this.drawableName = drawableName;

  get getTotalOrders => this.totalOrders;

  set setTotalOrders(totalOrders) => this.totalOrders = totalOrders;

  get getSnapshot => this.snapshot;

  set setSnapshot(snapshot) => this.snapshot = snapshot;

  get getReference => this.reference;

  set setReference(reference) => this.reference = reference;

  get getDocumentID => this.documentID;

  set setDocumentID(documentID) => this.documentID = documentID;

  ServiceType({
    this.id,
    this.name,
    this.drawableName,
    this.totalOrders,
    this.snapshot,
    this.reference,
    this.documentID,
  });

  factory ServiceType.fromFirestore(DocumentSnapshot snapshot) {
    if (snapshot == null) return null;
    var map = snapshot.data();

    return ServiceType(
      id: map['id'],
      name: map['name'],
      drawableName: map['drawableName'],
      totalOrders: map['totalOrders'],
      snapshot: snapshot,
      reference: snapshot.reference,
      documentID: snapshot.id,
    );
  }

  factory ServiceType.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return ServiceType(
      id: map['id'],
      name: map['name'],
      drawableName: map['drawableName'],
      totalOrders: map['totalOrders'],
    );
  }

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'drawableName': drawableName,
        'totalOrders': totalOrders,
      };

  ServiceType copyWith({
    String id,
    String name,
    String drawableName,
    int totalOrders,
  }) {
    return ServiceType(
      id: id ?? this.id,
      name: name ?? this.name,
      drawableName: drawableName ?? this.drawableName,
      totalOrders: totalOrders ?? this.totalOrders,
    );
  }

  @override
  String toString() {
    return '${id.toString()}, ${name.toString()}, ${drawableName.toString()}, ${totalOrders.toString()}, ';
  }

  @override
  bool operator ==(other) =>
      other is ServiceType && documentID == other.documentID;

  int get hashCode => documentID.hashCode;
}
