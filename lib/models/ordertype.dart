enum OrderType { QUICK_CALL, MAKE_AN_APPOINTMENT }

extension OrderTypeExtension on OrderType {
  String get title {
    switch (this) {
      case OrderType.QUICK_CALL:
        return "Gọi nhanh";
      case OrderType.MAKE_AN_APPOINTMENT:
        return "Đặt lịch";
      default:
        return null;
    }
  }

  static OrderType fromTitle(String title) {
    if (title.isNotEmpty) {
      for (OrderType orderStatus in OrderType.values) {
        if (orderStatus.title == title) {
          return orderStatus;
        }
      }
    }
    return null;
  }

  static OrderType getOrderTypeByTitle(String title) =>
      OrderType.values.firstWhere((v) => title == v.title, orElse: () => null);
}
