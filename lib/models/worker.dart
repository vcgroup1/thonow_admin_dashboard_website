import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:thonow_admin_dashboard_website/models/address.dart';
import 'package:thonow_admin_dashboard_website/models/creditcardinfo.dart';
import 'package:thonow_admin_dashboard_website/models/identitycard.dart';

class Worker {
  String id;
  String displayName;
  String email;
  String phoneNumber;
  String photoUrl;
  String residentAddress;
  Address address;
  double balanceMoney;
  String idNumber;
  String promoCode;
  double rating;
  bool male;
  bool online;
  bool active;
  bool verified;
  List<DocumentReference> serviceTypeList;
  List<CreditCardInfo> creditCardInfos;
  IdentityCard idCard;
  DateTime createdAt;
  DocumentSnapshot snapshot;
  DocumentReference reference;
  String documentID;

  DateTime get getCreatedAt => this.createdAt;

  set setCreatedAt(DateTime createdAt) => this.createdAt = createdAt;

  String get getId => this.id;

  set setId(String id) => this.id = id;

  get getDisplayName => this.displayName;

  set setDisplayName(displayName) => this.displayName = displayName;

  get getEmail => this.email;

  set setEmail(email) => this.email = email;

  get getPhoneNumber => this.phoneNumber;

  set setPhoneNumber(phoneNumber) => this.phoneNumber = phoneNumber;

  get getPhotoUrl => this.photoUrl;

  set setPhotoUrl(photoUrl) => this.photoUrl = photoUrl;

  get getResidentAddress => this.residentAddress;

  set setResidentAddress(residentAddress) =>
      this.residentAddress = residentAddress;

  get getAddress => this.address;

  set setAddress(address) => this.address = address;

  get getBalanceMoney => this.balanceMoney;

  set setBalanceMoney(balanceMoney) => this.balanceMoney = balanceMoney;

  get getIdNumber => this.idNumber;

  set setIdNumber(idNumber) => this.idNumber = idNumber;

  get getPromoCode => this.promoCode;

  set setPromoCode(promoCode) => this.promoCode = promoCode;

  get getRating => this.rating;

  set setRating(rating) => this.rating = rating;

  get getmale => this.male;

  set setmale(male) => this.male = male;

  get getonline => this.online;

  set setonline(online) => this.online = online;

  get getactive => this.active;

  set setactive(active) => this.active = active;

  get getverified => this.verified;

  set setverified(verified) => this.verified = verified;

  get getServiceTypeList => this.serviceTypeList;

  set setServiceTypeList(serviceTypeList) =>
      this.serviceTypeList = serviceTypeList;

  get getCreditCardInfos => this.creditCardInfos;

  set setCreditCardInfos(creditCardInfos) =>
      this.creditCardInfos = creditCardInfos;

  get getIdCard => this.idCard;

  set setIdCard(idCard) => this.idCard = idCard;

  get getSnapshot => this.snapshot;

  set setSnapshot(snapshot) => this.snapshot = snapshot;

  get getReference => this.reference;

  set setReference(reference) => this.reference = reference;

  get getDocumentID => this.documentID;

  set setDocumentID(documentID) => this.documentID = documentID;

  Worker({
    this.id,
    this.displayName,
    this.email,
    this.phoneNumber,
    this.photoUrl,
    this.residentAddress,
    this.address,
    this.balanceMoney,
    this.idNumber,
    this.promoCode,
    this.rating,
    this.male,
    this.online,
    this.active,
    this.verified,
    this.serviceTypeList,
    this.creditCardInfos,
    this.idCard,
    this.createdAt,
    this.snapshot,
    this.reference,
    this.documentID,
  });

  factory Worker.fromFirestore(DocumentSnapshot snapshot) {
    if (snapshot == null) return null;
    var map = snapshot.data();

    return Worker(
      id: map['id'],
      displayName: map['displayName'],
      email: map['email'],
      phoneNumber: map['phoneNumber'],
      photoUrl: map['photoUrl'],
      residentAddress: map['residentAddress'],
      address: map['address'] != null ? Address.fromMap(map['address']) : null,
      balanceMoney: map['balanceMoney'],
      idNumber: map['idNumber'],
      promoCode: map['promoCode'],
      rating: map['rating'],
      male: map['male'],
      online: map['online'],
      active: map['active'],
      verified: map['verified'],
      serviceTypeList: map['serviceTypeList'] != null
          ? List<DocumentReference>.from(map['serviceTypeList'])
          : null,
      creditCardInfos: map['creditCardInfos'] != null
          ? CreditCardInfo.fromListMap(
              List<Map<String, dynamic>>.from(map['creditCardInfos']))
          : null,
      idCard:
          map['idCard'] != null ? IdentityCard.fromMap(map['idCard']) : null,
      createdAt: map['createdAt'] != null
          ? DateTime.fromMicrosecondsSinceEpoch(
              map['createdAt'].millisecondsSinceEpoch)
          : null,
      snapshot: snapshot,
      reference: snapshot.reference,
      documentID: snapshot.id,
    );
  }

  factory Worker.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Worker(
      id: map['id'],
      displayName: map['displayName'],
      email: map['email'],
      phoneNumber: map['phoneNumber'],
      photoUrl: map['photoUrl'],
      residentAddress: map['residentAddress'],
      address: map['address'] != null ? Address.fromMap(map['address']) : null,
      balanceMoney: map['balanceMoney'],
      idNumber: map['idNumber'],
      promoCode: map['promoCode'],
      rating: map['rating'],
      male: map['male'],
      online: map['online'],
      active: map['active'],
      verified: map['verified'],
      serviceTypeList: map['serviceTypeList'] != null
          ? List<DocumentReference>.from(map['serviceTypeList'])
          : null,
      creditCardInfos: map['creditCardInfos'] != null
          ? CreditCardInfo.fromListMap(
              List<Map<String, dynamic>>.from(map['creditCardInfos']))
          : null,
      idCard:
          map['idCard'] != null ? IdentityCard.fromMap(map['idCard']) : null,
      createdAt: map['createdAt'] != null
          ? DateTime.fromMicrosecondsSinceEpoch(
              map['createdAt'].millisecondsSinceEpoch)
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'id': id,
        'displayName': displayName,
        'email': email,
        'phoneNumber': phoneNumber,
        'photoUrl': photoUrl,
        'residentAddress': residentAddress,
        'address': address,
        'balanceMoney': balanceMoney,
        'idNumber': idNumber,
        'promoCode': promoCode,
        'rating': rating,
        'male': male,
        'online': online,
        'active': active,
        'verified': verified,
        'serviceTypeList': serviceTypeList,
        'creditCardInfos': creditCardInfos,
        'idCard': idCard,
        'createdAt': createdAt,
      };

  Worker copyWith({
    String id,
    String displayName,
    String email,
    String phoneNumber,
    String photoUrl,
    String residentAddress,
    Address address,
    double balanceMoney,
    String idNumber,
    String promoCode,
    double rating,
    bool male,
    bool online,
    bool active,
    bool verified,
    List<DocumentReference> serviceTypeList,
    List<CreditCardInfo> creditCardInfos,
    IdentityCard idCard,
    DateTime createdAt,
  }) {
    return Worker(
      id: id ?? this.id,
      displayName: displayName ?? this.displayName,
      email: email ?? this.email,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      photoUrl: photoUrl ?? this.photoUrl,
      residentAddress: residentAddress ?? this.residentAddress,
      address: address ?? this.address,
      balanceMoney: balanceMoney ?? this.balanceMoney,
      idNumber: idNumber ?? this.idNumber,
      promoCode: promoCode ?? this.promoCode,
      rating: rating ?? this.rating,
      male: male ?? this.male,
      online: online ?? this.online,
      active: active ?? this.active,
      verified: verified ?? this.verified,
      serviceTypeList: serviceTypeList ?? this.serviceTypeList,
      creditCardInfos: creditCardInfos ?? this.creditCardInfos,
      idCard: idCard ?? this.idCard,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  @override
  String toString() {
    return '${id.toString()}, ${displayName.toString()}, ${email.toString()}, ${phoneNumber.toString()}, ${photoUrl.toString()}, ${residentAddress.toString()}, ${address.toString()}, ${balanceMoney.toString()}, ${idNumber.toString()}, ${promoCode.toString()}, ${rating.toString()}, ${male.toString()}, ${online.toString()}, ${active.toString()}, ${verified.toString()}, ${serviceTypeList.toString()}, ${creditCardInfos.toString()}, ${idCard.toString()}, ';
  }

  @override
  bool operator ==(other) => other is Worker && documentID == other.documentID;

  int get hashCode => documentID.hashCode;
}
