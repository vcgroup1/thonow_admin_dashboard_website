import 'dart:convert';

class MaterialCost {
  String name;
  double price;
  MaterialCost({
    this.name,
    this.price,
  });

  MaterialCost copyWith({
    String name,
    double price,
  }) {
    return MaterialCost(
      name: name ?? this.name,
      price: price ?? this.price,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'price': price,
    };
  }

  factory MaterialCost.fromMap(Map<String, dynamic> map) {
    return MaterialCost(
      name: map['name'],
      price: map['price'],
    );
  }

  String toJson() => json.encode(toMap());

  factory MaterialCost.fromJson(String source) =>
      MaterialCost.fromMap(json.decode(source));

  @override
  String toString() => 'MaterialCost(name: $name, price: $price)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MaterialCost && other.name == name && other.price == price;
  }

  @override
  int get hashCode => name.hashCode ^ price.hashCode;

  static List<MaterialCost> fromListMap(List<Map<String, dynamic>> listMap) {
    if (listMap != null && listMap.isNotEmpty) {
      List<MaterialCost> results = [];
      listMap.forEach((element) {
        results.add(MaterialCost.fromMap(element));
      });
      return results;
    }
    return null;
  }
}
