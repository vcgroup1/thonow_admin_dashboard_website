import 'dart:convert';

class Address {
  double latitude;
  double longtitude;
  String address;
  Address({
    this.latitude,
    this.longtitude,
    this.address,
  });

  Address copyWith({
    double latitude,
    double longtitude,
    String address,
  }) {
    return Address(
      latitude: latitude ?? this.latitude,
      longtitude: longtitude ?? this.longtitude,
      address: address ?? this.address,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'latitude': latitude,
      'longtitude': longtitude,
      'address': address,
    };
  }

  factory Address.fromMap(Map<String, dynamic> map) {
    return Address(
      latitude: map['latitude'],
      longtitude: map['longtitude'],
      address: map['address'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Address.fromJson(String source) =>
      Address.fromMap(json.decode(source));

  @override
  String toString() =>
      'Address(latitude: $latitude, longtitude: $longtitude, address: $address)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Address &&
        other.latitude == latitude &&
        other.longtitude == longtitude &&
        other.address == address;
  }

  @override
  int get hashCode =>
      latitude.hashCode ^ longtitude.hashCode ^ address.hashCode;
}
