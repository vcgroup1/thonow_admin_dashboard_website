import 'dart:convert';

class IdentityCard {
  String frontIdCardUrl;
  String backIdCardUrl;
  IdentityCard({
    this.frontIdCardUrl,
    this.backIdCardUrl,
  });

  Map<String, dynamic> toMap() {
    return {
      'frontIdCardUrl': frontIdCardUrl,
      'backIdCardUrl': backIdCardUrl,
    };
  }

  factory IdentityCard.fromMap(Map<String, dynamic> map) {
    return IdentityCard(
      frontIdCardUrl: map['frontIdCardUrl'],
      backIdCardUrl: map['backIdCardUrl'],
    );
  }

  String toJson() => json.encode(toMap());

  factory IdentityCard.fromJson(String source) =>
      IdentityCard.fromMap(json.decode(source));

  @override
  String toString() =>
      'IdentityCard(frontIdCardUrl: $frontIdCardUrl, backIdCardUrl: $backIdCardUrl)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is IdentityCard &&
        other.frontIdCardUrl == frontIdCardUrl &&
        other.backIdCardUrl == backIdCardUrl;
  }

  @override
  int get hashCode => frontIdCardUrl.hashCode ^ backIdCardUrl.hashCode;
}
