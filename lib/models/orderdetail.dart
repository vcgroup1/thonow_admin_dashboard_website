
import 'package:cloud_firestore/cloud_firestore.dart';

class OrderDetail {
  final double unitPrice;
  final int quantity;
  final DocumentReference service;
  final double subtotal;
  final DocumentSnapshot snapshot;
  final DocumentReference reference;
  final String documentID;

  OrderDetail({
    this.unitPrice,
    this.quantity,
    this.service,
    this.subtotal,
    this.snapshot,
    this.reference,
    this.documentID,
  });

  factory OrderDetail.fromFirestore(DocumentSnapshot snapshot) {
    if (snapshot == null) return null;
    var map = snapshot.data();

    return OrderDetail(
      unitPrice: map['unitPrice'],
      quantity: map['quantity'],
      service: map['service'],
      subtotal: map['subtotal'],
      snapshot: snapshot,
      reference: snapshot.reference,
      documentID: snapshot.id,
    );
  }

  factory OrderDetail.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return OrderDetail(
      unitPrice: map['unitPrice'],
      quantity: map['quantity'],
      service: map['service'],
      subtotal: map['subtotal'],
    );
  }

  Map<String, dynamic> toMap() => {
        'unitPrice': unitPrice,
        'quantity': quantity,
        'service': service,
        'subtotal': subtotal,
      };

  OrderDetail copyWith({
    double unitPrice,
    int quantity,
    DocumentReference service,
    double subtotal,
  }) {
    return OrderDetail(
      unitPrice: unitPrice ?? this.unitPrice,
      quantity: quantity ?? this.quantity,
      service: service ?? this.service,
      subtotal: subtotal ?? this.subtotal,
    );
  }

  @override
  String toString() {
    return '${unitPrice.toString()}, ${quantity.toString()}, ${service.toString()}, ${subtotal.toString()}, ';
  }

  @override
  bool operator ==(other) =>
      other is OrderDetail && documentID == other.documentID;

  int get hashCode => documentID.hashCode;

  static List<OrderDetail> fromListMap(List<Map<String, dynamic>> listMap) {
    if (listMap != null && listMap.isNotEmpty) {
      List<OrderDetail> results = [];
      listMap.forEach((element) {
        results.add(OrderDetail.fromMap(element));
      });
      return results;
    }
    return null;
  }
}
