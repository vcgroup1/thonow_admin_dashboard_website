import 'package:cloud_firestore/cloud_firestore.dart';

class Service {
  String id;
  String name;
  String unit;
  double price;
  int warrantyPeriod;
  DocumentReference serviceType;
  DocumentSnapshot snapshot;
  DocumentReference reference;
  String documentID;
  String get getId => this.id;

  set setId(String id) => this.id = id;

  get getName => this.name;

  set setName(name) => this.name = name;

  get getUnit => this.unit;

  set setUnit(unit) => this.unit = unit;

  get getPrice => this.price;

  set setPrice(price) => this.price = price;

  get getWarrantyPeriod => this.warrantyPeriod;

  set setWarrantyPeriod(warrantyPeriod) => this.warrantyPeriod = warrantyPeriod;

  get getServiceType => this.serviceType;

  set setServiceType(serviceType) => this.serviceType = serviceType;

  get getSnapshot => this.snapshot;

  set setSnapshot(snapshot) => this.snapshot = snapshot;

  get getReference => this.reference;

  set setReference(reference) => this.reference = reference;

  get getDocumentID => this.documentID;

  set setDocumentID(documentID) => this.documentID = documentID;

  Service({
    this.id,
    this.name,
    this.unit,
    this.price,
    this.warrantyPeriod,
    this.serviceType,
    this.snapshot,
    this.reference,
    this.documentID,
  });

  factory Service.fromFirestore(DocumentSnapshot snapshot) {
    if (snapshot == null) return null;
    var map = snapshot.data();

    return Service(
      id: map['id'],
      name: map['name'],
      unit: map['unit'],
      price: map['price'],
      warrantyPeriod: map['warrantyPeriod'],
      serviceType: map['serviceType'],
      snapshot: snapshot,
      reference: snapshot.reference,
      documentID: snapshot.id,
    );
  }

  factory Service.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Service(
      id: map['id'],
      name: map['name'],
      unit: map['unit'],
      price: map['price'],
      warrantyPeriod: map['warrantyPeriod'],
      serviceType: map['serviceType'],
    );
  }

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'unit': unit,
        'price': price,
        'warrantyPeriod': warrantyPeriod,
        'serviceType': serviceType,
      };

  Service copyWith({
    String id,
    String name,
    String unit,
    double price,
    int warrantyPeriod,
    DocumentReference serviceType,
  }) {
    return Service(
      id: id ?? this.id,
      name: name ?? this.name,
      unit: unit ?? this.unit,
      price: price ?? this.price,
      warrantyPeriod: warrantyPeriod ?? this.warrantyPeriod,
      serviceType: serviceType ?? this.serviceType,
    );
  }

  @override
  String toString() {
    return '${id.toString()}, ${name.toString()}, ${unit.toString()}, ${price.toString()}, ${warrantyPeriod.toString()}, ${serviceType.toString()}, ';
  }

  @override
  bool operator ==(other) => other is Service && documentID == other.documentID;

  int get hashCode => documentID.hashCode;
}
