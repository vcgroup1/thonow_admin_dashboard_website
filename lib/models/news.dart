import 'package:cloud_firestore/cloud_firestore.dart';

class News {
  String id;
  String title;
  DateTime createdAt;
  String thumbnail;
  String content;
  bool isForWorker;
  DocumentSnapshot snapshot;
  DocumentReference reference;
  String documentID;

  News({
    this.id,
    this.title,
    this.createdAt,
    this.thumbnail,
    this.content,
    this.isForWorker,
    this.snapshot,
    this.reference,
    this.documentID,
  });

  String get getId => this.id;

  set setId(String id) => this.id = id;

  get getTitle => this.title;

  set setTitle(String title) {
    this.title = title;
  }

  get getCreatedAt => this.createdAt;

  set setCreatedAt(createdAt) => this.createdAt = createdAt;

  get getThumbnail => this.thumbnail;

  set setThumbnail(thumbnail) => this.thumbnail = thumbnail;

  get getContent => this.content;

  set setContent(content) => this.content = content;

  get getIsForWorker => this.isForWorker;

  set setIsForWorker(isForWorker) => this.isForWorker = isForWorker;

  factory News.fromFirestore(DocumentSnapshot snapshot) {
    if (snapshot == null) return null;
    var map = snapshot.data();

    return News(
      id: map['id'],
      title: map['title'],
      createdAt: map['createdAt']?.toDate(),
      thumbnail: map['thumbnail'],
      content: map['content'],
      isForWorker: map['isForWorker'],
      snapshot: snapshot,
      reference: snapshot.reference,
      documentID: snapshot.id,
    );
  }

  factory News.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return News(
      id: map['id'],
      title: map['title'],
      createdAt: map['createdAt']?.toDate(),
      thumbnail: map['thumbnail'],
      content: map['content'],
      isForWorker: map['isForWorker'],
    );
  }

  Map<String, dynamic> toMap() => {
        'id': id,
        'title': title,
        'createdAt': createdAt,
        'thumbnail': thumbnail,
        'content': content,
        'isForWorker': isForWorker,
      };

  News copyWith({
    String id,
    String title,
    DateTime createdAt,
    String thumbnail,
    String content,
    bool isForWorker,
  }) {
    return News(
      id: id ?? this.id,
      title: title ?? this.title,
      createdAt: createdAt ?? this.createdAt,
      thumbnail: thumbnail ?? this.thumbnail,
      content: content ?? this.content,
      isForWorker: isForWorker ?? this.isForWorker,
    );
  }

  @override
  String toString() {
    return '${id.toString()}, ${title.toString()}, ${createdAt.toString()}, ${thumbnail.toString()}, ${content.toString()}, ${isForWorker.toString()}, ';
  }

  @override
  bool operator ==(other) => other is News && documentID == other.documentID;

  int get hashCode => documentID.hashCode;
}
