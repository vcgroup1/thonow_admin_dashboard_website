import 'dart:convert';

class Feedback {
  double rating;
  String review;
  Feedback({
    this.rating,
    this.review,
  });

  Feedback copyWith({
    double rating,
    String review,
  }) {
    return Feedback(
      rating: rating ?? this.rating,
      review: review ?? this.review,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'rating': rating,
      'review': review,
    };
  }

  factory Feedback.fromMap(Map<String, dynamic> map) {
    return Feedback(
      rating: map['rating'],
      review: map['review'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Feedback.fromJson(String source) =>
      Feedback.fromMap(json.decode(source));

  @override
  String toString() => 'Feedback(rating: $rating, review: $review)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Feedback &&
        other.rating == rating &&
        other.review == review;
  }

  @override
  int get hashCode => rating.hashCode ^ review.hashCode;
}
