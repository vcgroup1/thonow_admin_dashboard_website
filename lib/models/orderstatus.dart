enum OrderStatus {
  ALLOCATING,
  WAITING_FOR_WORK,
  ON_THE_MOVE,
  ARRIVED,
  INVESTIGATING,
  WAIT_FOR_ORDER_CONFIRMATION,
  CONFIRMED_ORDER,
  WORKING,
  WAIT_FOR_CONFIRMATION_TO_COMPLETE,
  WAIT_FOR_CONFIRMATION_TO_PAY,
  COMPLETED,
  CANCELED
}

extension OrderStatusExtension on OrderStatus {
  String get title {
    switch (this) {
      case OrderStatus.ALLOCATING:
        return "Đang tìm thợ";
      case OrderStatus.WAITING_FOR_WORK:
        return "Đang đợi làm việc";
      case OrderStatus.ON_THE_MOVE:
        return "Đang di chuyển";
      case OrderStatus.ARRIVED:
        return "Đã đến nơi";
      case OrderStatus.INVESTIGATING:
        return "Đang khảo sát";
      case OrderStatus.WAIT_FOR_ORDER_CONFIRMATION:
        return "Chờ xác nhận đơn";
      case OrderStatus.CONFIRMED_ORDER:
        return "Đơn đã xác nhận";
      case OrderStatus.WORKING:
        return "Đang làm việc";
      case OrderStatus.WAIT_FOR_CONFIRMATION_TO_COMPLETE:
        return "Chờ xác nhận hoàn thành";
      case OrderStatus.WAIT_FOR_CONFIRMATION_TO_PAY:
        return "Chờ xác nhận thanh toán";
      case OrderStatus.COMPLETED:
        return "Đã hoàn thành";
      case OrderStatus.CANCELED:
        return "Đơn đã bị từ chối";
      default:
        return null;
    }
  }

  String get color {
    switch (this) {
      case OrderStatus.ALLOCATING:
        return "#3298dc";
      case OrderStatus.WAITING_FOR_WORK:
        return "#3298dc";
      case OrderStatus.ON_THE_MOVE:
        return "#17a2b8";
      case OrderStatus.ARRIVED:
        return "#FFA900";
      case OrderStatus.INVESTIGATING:
        return "#39C0ED";
      case OrderStatus.WAIT_FOR_ORDER_CONFIRMATION:
        return "#E65100";
      case OrderStatus.CONFIRMED_ORDER:
        return "#827717";
      case OrderStatus.WORKING:
        return "#006064";
      case OrderStatus.WAIT_FOR_CONFIRMATION_TO_COMPLETE:
        return "#1A237E";
      case OrderStatus.WAIT_FOR_CONFIRMATION_TO_PAY:
        return "#0091EA";
      case OrderStatus.COMPLETED:
        return "#48c774";
      case OrderStatus.CANCELED:
        return "#f14668";
      default:
        return null;
    }
  }

  OrderStatus getOrderStatusByTitle(String title) => OrderStatus.values
      .firstWhere((v) => title == v.title, orElse: () => null);
}
