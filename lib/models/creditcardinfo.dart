import 'dart:convert';

class CreditCardInfo {
  final String accountNumber;
  final String accountName;
  final String bankName;
  final String branchName;
  CreditCardInfo({
    this.accountNumber,
    this.accountName,
    this.bankName,
    this.branchName,
  });

  CreditCardInfo copyWith({
    String accountNumber,
    String accountName,
    String bankName,
    String branchName,
  }) {
    return CreditCardInfo(
      accountNumber: accountNumber ?? this.accountNumber,
      accountName: accountName ?? this.accountName,
      bankName: bankName ?? this.bankName,
      branchName: branchName ?? this.branchName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'accountNumber': accountNumber,
      'accountName': accountName,
      'bankName': bankName,
      'branchName': branchName,
    };
  }

  factory CreditCardInfo.fromMap(Map<String, dynamic> map) {
    return CreditCardInfo(
      accountNumber: map['accountNumber'],
      accountName: map['accountName'],
      bankName: map['bankName'],
      branchName: map['branchName'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CreditCardInfo.fromJson(String source) =>
      CreditCardInfo.fromMap(json.decode(source));

  @override
  String toString() {
    return 'CreditCardInfo(accountNumber: $accountNumber, accountName: $accountName, bankName: $bankName, branchName: $branchName)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CreditCardInfo &&
        other.accountNumber == accountNumber &&
        other.accountName == accountName &&
        other.bankName == bankName &&
        other.branchName == branchName;
  }

  @override
  int get hashCode {
    return accountNumber.hashCode ^
        accountName.hashCode ^
        bankName.hashCode ^
        branchName.hashCode;
  }

  static List<CreditCardInfo> fromListMap(List<Map<String, dynamic>> listMap) {
    if (listMap != null && listMap.isNotEmpty) {
      List<CreditCardInfo> results = [];
      listMap.forEach((element) {
        results.add(CreditCardInfo.fromMap(element));
      });
      return results;
    }
    return null;
  }
}
